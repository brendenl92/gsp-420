uniform extern texture LightMap;
uniform extern float dim;

sampler finalSampler = sampler_state
{
	Texture = <LightMap>;
	MagFilter = Point;
	MinFilter = Point;
};

struct FinalInput
{
	float2 tex0 : TEXCOORD0;
};

float4 FinalPS(FinalInput i) : COLOR0
{
	return float4(tex2D(finalSampler, i.tex0).rgb - float3(dim, dim, dim), 1);
}
	
technique main
{
	pass p0
	{
		VertexShader = NULL;
		PixelShader = compile ps_2_0 FinalPS();
	}

};