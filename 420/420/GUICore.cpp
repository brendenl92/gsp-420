#include "GUICore.h"
#include "Input.h"
#include "ResourceManager.h"
#include "Kernel.h"

GUICore* GUICore::Instance()
{
	static GUICore* temp = new GUICore();
	return temp;
}

void GUICore::Initialize()
{
	string name[3];

	name[0] = "../Media/Textures/ExampleButtons512x512.png";
	name[1] = "../Media/Textures/SpaceTheme.jpg";
	name[2] = "../Media/Textures/RobinAnimation.png";

	KRNL->GetRMPtr()->ImportTextures(name, 3);

	KRNL->GetRMPtr()->createFont("Ariel", 20, 10, FW_BOLD, true);

	SetRect(&ButtonRect, 0, 0, 128, 32);
	SetRect(&HoverButtonRect, 128, 32, 256, 64);
}

void GUICore::StartUp()
{
	string name[3];

	name[0] = "../Media/Textures/ExampleButtons512x512.png";
	name[1] = "../Media/Textures/SpaceTheme.jpg";
	name[2] = "../Media/Textures/RobinAnimation.png";

	KRNL->GetRMPtr()->ImportTextures(name, 3);

	KRNL->GetRMPtr()->createFont("Ariel", 20, 10, FW_BOLD, false);

	SetRect(&ButtonRect, 0, 0, 128, 32);
	SetRect(&HoverButtonRect, 128, 0, 256, 32);

	MousePos = &D3DXVECTOR4(0, 0, 0, 0);
}

void GUICore::Run()
{

}

void GUICore::Draw()
{
	//Sprite Begin
	KRNL->GetRMPtr()->getSprite()->Begin(D3DXSPRITE_ALPHABLEND);

	D3DXMatrixTransformation2D(&mMatrix, &D3DXVECTOR2(0, 0), 0, &D3DXVECTOR2(1, 1), NULL, 0, &D3DXVECTOR2(0, 0));
	KRNL->GetRMPtr()->getSprite()->SetTransform(&mMatrix);
	KRNL->GetRMPtr()->getSprite()->Draw(*KRNL->GetRMPtr()->getTexture(1), NULL, &D3DXVECTOR3(0, 0, 0), &D3DXVECTOR3(0, 0, 0), D3DXCOLOR(1, 1, 1, 1));

	//Campaign Button
	D3DXMatrixTransformation2D(&mMatrix, &D3DXVECTOR2(250, 0), 0, &D3DXVECTOR2(2, 2), NULL, 0, &D3DXVECTOR2((float)KRNL->GetGraphicsPtr()->GetWindowWidth() / 4, (float)(KRNL->GetGraphicsPtr()->GetWindowHeight() / 4) * 3));
	KRNL->GetRMPtr()->getSprite()->SetTransform(&mMatrix);
	KRNL->GetRMPtr()->getSprite()->Draw(*KRNL->GetRMPtr()->getTexture(0), &ButtonRect, &D3DXVECTOR3(0, 0, 0), &D3DXVECTOR3(0, 0, 0), D3DXCOLOR(1, 1, 1, 1));

	if (MousePos->z < KRNL->GetGraphicsPtr()->GetWindowWidth() / 4 && MousePos->z >(KRNL->GetGraphicsPtr()->GetWindowWidth() / 4) - 250
		&& MousePos->w > (KRNL->GetGraphicsPtr()->GetWindowHeight() / 4) * 3 && MousePos->w < ((KRNL->GetGraphicsPtr()->GetWindowHeight() / 4) * 3) + 65)
	{
		KRNL->GetRMPtr()->getSprite()->Draw(*KRNL->GetRMPtr()->getTexture(0), &HoverButtonRect, &D3DXVECTOR3(0, 0, 0), &D3DXVECTOR3(0, 0, 0), D3DXCOLOR(1, 1, 1, 1));

		if (IsMouseDown == (char*)1)
		{
			KRNL->ChangeState(new GameState());
			return;
		}
	}

	KRNL->GetRMPtr()->getFont(0)->DrawText(KRNL->GetRMPtr()->getSprite(), "Campaign", -1, &ButtonRect, DT_CENTER | DT_VCENTER, D3DCOLOR_XRGB(255, 255, 255));

	//Multiplayer Button
	D3DXMatrixTransformation2D(&mMatrix, &D3DXVECTOR2(250, 0), 0, &D3DXVECTOR2(2, 2), NULL, 0, &D3DXVECTOR2((float)KRNL->GetGraphicsPtr()->GetWindowWidth() / 4, (float)((KRNL->GetGraphicsPtr()->GetWindowHeight() / 4) * 3) + 100));
	KRNL->GetRMPtr()->getSprite()->SetTransform(&mMatrix);
	KRNL->GetRMPtr()->getSprite()->Draw(*KRNL->GetRMPtr()->getTexture(0), &ButtonRect, &D3DXVECTOR3(0, 0, 0), &D3DXVECTOR3(0, 0, 0), D3DXCOLOR(1, 1, 1, 1));

	if (MousePos->z < KRNL->GetGraphicsPtr()->GetWindowWidth() / 4 && MousePos->z >(KRNL->GetGraphicsPtr()->GetWindowWidth() / 4) - 250
		&& MousePos->w > ((KRNL->GetGraphicsPtr()->GetWindowHeight() / 4) * 3) + 100 && MousePos->w < ((KRNL->GetGraphicsPtr()->GetWindowHeight() / 4) * 3) + 165)
		KRNL->GetRMPtr()->getSprite()->Draw(*KRNL->GetRMPtr()->getTexture(0), &HoverButtonRect, &D3DXVECTOR3(0, 0, 0), &D3DXVECTOR3(0, 0, 0), D3DXCOLOR(1, 1, 1, 1));

	KRNL->GetRMPtr()->getFont(0)->DrawText(KRNL->GetRMPtr()->getSprite(), "Multiplayer", -1, &ButtonRect, DT_CENTER | DT_VCENTER, D3DCOLOR_XRGB(255, 255, 255));

	//Options Button
	D3DXMatrixTransformation2D(&mMatrix, &D3DXVECTOR2(0, 0), 0, &D3DXVECTOR2(2, 2), NULL, 0, &D3DXVECTOR2((float)(KRNL->GetGraphicsPtr()->GetWindowWidth() / 4) * 3, (float)(KRNL->GetGraphicsPtr()->GetWindowHeight() / 4) * 3));
	KRNL->GetRMPtr()->getSprite()->SetTransform(&mMatrix);
	KRNL->GetRMPtr()->getSprite()->Draw(*KRNL->GetRMPtr()->getTexture(0), &ButtonRect, &D3DXVECTOR3(0, 0, 0), &D3DXVECTOR3(0, 0, 0), D3DXCOLOR(1, 1, 1, 1));

	if (MousePos->z >(KRNL->GetGraphicsPtr()->GetWindowWidth() / 4) * 3 && MousePos->z < ((KRNL->GetGraphicsPtr()->GetWindowWidth() / 4) * 3) + 250
		&& MousePos->w >(KRNL->GetGraphicsPtr()->GetWindowHeight() / 4) * 3 && MousePos->w < ((KRNL->GetGraphicsPtr()->GetWindowHeight() / 4) * 3) + 65)
		KRNL->GetRMPtr()->getSprite()->Draw(*KRNL->GetRMPtr()->getTexture(0), &HoverButtonRect, &D3DXVECTOR3(0, 0, 0), &D3DXVECTOR3(0, 0, 0), D3DXCOLOR(1, 1, 1, 1));

	KRNL->GetRMPtr()->getFont(0)->DrawText(KRNL->GetRMPtr()->getSprite(), "Options", -1, &ButtonRect, DT_CENTER | DT_VCENTER, D3DCOLOR_XRGB(255, 255, 255));

	//Quit Button
	D3DXMatrixTransformation2D(&mMatrix, &D3DXVECTOR2(0, 0), 0, &D3DXVECTOR2(2, 2), NULL, 0, &D3DXVECTOR2((float)(KRNL->GetGraphicsPtr()->GetWindowWidth() / 4) * 3, (float)((KRNL->GetGraphicsPtr()->GetWindowHeight() / 4) * 3) + 100));
	KRNL->GetRMPtr()->getSprite()->SetTransform(&mMatrix);
	KRNL->GetRMPtr()->getSprite()->Draw(*KRNL->GetRMPtr()->getTexture(0), &ButtonRect, &D3DXVECTOR3(0, 0, 0), &D3DXVECTOR3(0, 0, 0), D3DXCOLOR(1, 1, 1, 1));

	if (MousePos->z >(KRNL->GetGraphicsPtr()->GetWindowWidth() / 4) * 3 && MousePos->z < ((KRNL->GetGraphicsPtr()->GetWindowWidth() / 4) * 3) + 250
		&& MousePos->w >((KRNL->GetGraphicsPtr()->GetWindowHeight() / 4) * 3) + 100 && MousePos->w < ((KRNL->GetGraphicsPtr()->GetWindowHeight() / 4) * 3) + 165)
		KRNL->GetRMPtr()->getSprite()->Draw(*KRNL->GetRMPtr()->getTexture(0), &HoverButtonRect, &D3DXVECTOR3(0, 0, 0), &D3DXVECTOR3(0, 0, 0), D3DXCOLOR(1, 1, 1, 1));

	KRNL->GetRMPtr()->getFont(0)->DrawText(KRNL->GetRMPtr()->getSprite(), "Quit", -1, &ButtonRect, DT_CENTER | DT_VCENTER, D3DCOLOR_XRGB(255, 255, 255));

	/*D3DXMatrixTransformation2D(&mMatrix, &D3DXVECTOR2(0, 0), 0, &D3DXVECTOR2(1, 1), NULL, 0, &D3DXVECTOR2(0, 0));
	KRNL->GetRMPtr()->getSprite()->SetTransform(&mMatrix);
	KRNL->GetRMPtr()->getSprite()->Draw(*KRNL->GetRMPtr()->getTexture(2), NULL, &D3DXVECTOR3(0, 0, 0), &D3DXVECTOR3(0, 0, 0), D3DXCOLOR(1, 1, 1, 1));*/

	//Sprite End
	KRNL->GetRMPtr()->getSprite()->End();
}

void GUICore::ShutDown()
{
	KRNL->GetRMPtr()->clearAll();
}

void GUICore::CreateButton()
{

}