 #pragma once

#include <vector>
#include "BoundingVolume.h"



enum Priority_Type{LHS, RHS};

//////////////////////////
//Contact
/////////////////////////
class Contact
{
public:

	Contact(BoundingVolumeNode* lhs, BoundingVolumeNode* rhs, V3 PointOfContact, V3 Normal, float PenetrationDepth, float Epsilon = 1.0f);

	//Variables

	BoundingVolumeNode* mVolumes[2];
	float mDepth;
	float mEpsilon;
	V3 mPoint;
	V3 mNormal;
};



////////////////////////
//Collision Detector
////////////////////////
struct CollisionDetector
{
	bool DetectCollisions(BoundingVolumeNode* lhs, BoundingVolumeNode* rhs);
	bool SphereToSphere(BoundingVolumeNode* lhs, BoundingVolumeNode* rhs);
	bool SphereInsideAABB(BoundingVolumeNode* lhs, BoundingVolumeNode* rhs);
};



//////////////////////
//Contact Generator
/////////////////////
struct ContactGenerator
{
	Contact* GenerateContact(BoundingVolumeNode* lhs, BoundingVolumeNode* rhs);
	Contact* SphereToSphere(BoundingVolumeNode* Sphere, BoundingVolumeNode* Box);
};




/////////////////////////
//Contact Resolver
////////////////////////
struct ContactResolver
{
	void InterpenetrationFix(Contact* Contact);
};



///////////////////////////
//Collision Data
//////////////////////////

#define CONTACT CollisionData::Instance()

class CollisionData
{
public:
	CollisionData(int MaxContacts = 10, int MaxItterations = 10);

public:

	static CollisionData* Instance();

	CollisionDetector mDetector;
	ContactGenerator mGenerator;
	ContactResolver mResolver;


	//VAriables
	std::vector<Contact*> mContacts;

	int mMaxContacts;
	int mMaxItterations;

	//Methods
	void AddContact(Contact* contact);
	bool DetectCollision(BoundingVolumeNode* lhs, BoundingVolumeNode* rhs);
	Contact* GenerateContact(BoundingVolumeNode* lhs, BoundingVolumeNode* rhs);
	void RsolveContacts();
};