//SteeringManager.cpp
#include "Entity.h"
#include "Kernel.h"
#include "AIManager.h"
#include "SteeringManager.h"

D3DXVECTOR3 SteeringManager::CalculateSForce(int ID, Entity* target)
{
	//clear steering force
	m_SteeringForce = D3DXVECTOR3(0, 0, 0);

	switch (ID)
	{
	case 0:
	{
			  ///Seek
			  GetSeek(target->GetPosition());
			  break;
	}
	case 1:
	{
			  ///Flee
			  GetFlee(target->GetPosition());
			  break;
	}
	case 2:
	{
			  ///Pursue
			  Pursue(target);
			  break;
	}
	case 3:
	{
			  ///Evade
			  Evade(target);
			  break;
	}
	case 4:
	{
			  ///Could have the option to follow a path if needed.
			  break;
	}
	default:
			  break;
	}

	return m_SteeringForce;
}

D3DXVECTOR3 SteeringManager::Seek(D3DXVECTOR3 TargetPos)
{
	D3DXVECTOR3 DesiredVelocity = (TargetPos - SBase->GetPosition()) * SBase->AI_Base->maxSpeed * 2;
	return *D3DXVec3Normalize(&DesiredVelocity, &(DesiredVelocity - SBase->GetVelocity()));
}

D3DXVECTOR3 SteeringManager::Seek(Entity* Target)
{
	D3DXVECTOR3 DesiredVelocity = (Target->GetPosition() - SBase->GetPosition()) * SBase->AI_Base->maxSpeed * 2;
	return *D3DXVec3Normalize(&DesiredVelocity, &(DesiredVelocity - SBase->GetVelocity()));
}

D3DXVECTOR3 SteeringManager::GetSeek(D3DXVECTOR3 target)
{
	m_SteeringForce = D3DXVECTOR3(0, 0, 0);

	//Create feelers
	//CreateFeeler();

	///Obstacle Avoidance
	m_SteeringForce += ObstacleAvoidance();

	///Wall Avoidance
	//m_SteeringForce += WallAvoidance();

	///Seek
	m_SteeringForce += Seek(target);

	///Entity Avoidance
	m_SteeringForce += EntityAvoidance();

	m_SteeringForce.y = 0;

	return *D3DXVec3Normalize(&m_SteeringForce, &(m_SteeringForce * (m_WallWeight + m_ObstacleWeight + m_SeekWeight)));
}

D3DXVECTOR3 SteeringManager::Flee(D3DXVECTOR3 TargetPos)
{
	D3DXVECTOR3 DesiredVelocity = *D3DXVec3Normalize(&DesiredVelocity, &(SBase->GetPosition() - TargetPos)) * SBase->AI_Base->maxSpeed * 2;
	return (DesiredVelocity - SBase->GetVelocity());
}

D3DXVECTOR3 SteeringManager::Flee(Entity* Target)
{
	D3DXVECTOR3 DesiredVelocity = *D3DXVec3Normalize(&DesiredVelocity, &(SBase->GetPosition() - Target->GetPosition())) * SBase->AI_Base->maxSpeed * 2;
	return (DesiredVelocity - SBase->GetVelocity());
}

D3DXVECTOR3 SteeringManager::GetFlee(D3DXVECTOR3 target)
{
	m_SteeringForce = D3DXVECTOR3(0, 0, 0);

	//Create feelers
	//CreateFeeler();

	///Obstacle Avoidance
	m_SteeringForce += ObstacleAvoidance();

	///Wall Avoidance
	//m_SteeringForce += WallAvoidance();

	///Flee
	m_SteeringForce += Flee(target);

	///Entity Avoidance
	m_SteeringForce += EntityAvoidance();

	m_SteeringForce.y = 0;

	return *D3DXVec3Normalize(&m_SteeringForce, &(m_SteeringForce * (m_WallWeight + m_ObstacleWeight + m_FleeWeight)));
}

/*D3DXVECTOR3 SteeringManager::Wander()
{
	///need to select a point in between -1 and 1 will determine the 
	///direction I will be traveling in

	///start figuring out my wander point
	m_Wander = D3DXVECTOR3(Rand->Uniform(-1, 1) * GetWanderJitter(), 0, Rand->Uniform(-1, 1) * GetWanderJitter()).normalize();

	///change to world coordinates
	m_Wander.setX(m_Wander.getX() + SBase->Base->getPosition().getX() * .2);
	m_Wander.setZ(m_Wander.getZ() + SBase->Base->getPosition().getZ() * .2);

	///multiply by radius
	m_Wander *= GetWanderRadius();

	///getting distance from AI Objects in direction I'm traveling
	D3DXVECTOR3 n1 = (SBase->Base->getVelocity().normalize()) * GetWanderDistance();

	///combine the distance and my wander point
	m_Wander += n1;

	m_Wander.setY(0);

	///then return the steering vector that we are desiring to reach
	return (m_Wander - SBase->Base->getPosition());
}*/

D3DXVECTOR3 SteeringManager::GetWander()
{
	m_SteeringForce = D3DXVECTOR3(0, 0, 0);

	//Create feelers
	//CreateFeeler();

	///Wander
	//m_SteeringForce += Wander() / 2;

	///Wall Avoidance
	//m_SteeringForce += WallAvoidance();

	///Obstacle Avoidance
	m_SteeringForce += ObstacleAvoidance();

	///Entity Avoidance
	m_SteeringForce += EntityAvoidance();

	m_SteeringForce.y = 0;

	return *D3DXVec3Normalize(&m_SteeringForce, &(m_SteeringForce * (m_WallWeight + m_ObstacleWeight + m_WanderWeight)));
}

D3DXVECTOR3 SteeringManager::Arrive(Entity* Target)
{
	///Get distance from the object
	FLOAT distance = D3DXVec3Length(&(Target->GetPosition() - SBase->GetPosition()));

	///based on distance from point we will decrement the speed of the entity
	if (distance > 0)
	{
		///tweak the deceleration
		FLOAT dc = 0.45f;

		///calc speed needed
		FLOAT speed = distance / 4 * dc;

		///make sure velocity doesn't go over max speed
		if (speed > SBase->AI_Base->maxSpeed)
			speed = SBase->AI_Base->maxSpeed;

		D3DXVECTOR3 DesiredVelocity = (Target->GetPosition() - SBase->GetPosition()) * speed;

		return (DesiredVelocity - SBase->GetVelocity());
	}

	return D3DXVECTOR3(0, 0, 0);
}

D3DXVECTOR3 SteeringManager::Arrive(D3DXVECTOR3 TargetPos)
{
	///Get distance from object
	FLOAT distance = D3DXVec3Length(&(TargetPos - SBase->GetPosition()));

	///based on distance from point we will decrement the speed of the entity
	if (distance > 0)
	{
		///tweak the deceleration
		FLOAT dc = 0.45f;

		///calc speed needed
		FLOAT speed = distance / 4 * dc;

		///make sure velocity doesn't go over max speed
		if (speed > SBase->AI_Base->maxSpeed)
			speed = SBase->AI_Base->maxSpeed;

		D3DXVECTOR3 DesiredVelocity = (TargetPos - SBase->GetPosition()) * speed;

		return (DesiredVelocity - SBase->GetVelocity());
	}

	return D3DXVECTOR3(0, 0, 0);
}

D3DXVECTOR3 SteeringManager::GertArrive(D3DXVECTOR3 target)
{
	m_SteeringForce = D3DXVECTOR3(0, 0, 0);

	//Create feelers
	//CreateFeeler();

	///Wall Avoidance
	//m_SteeringForce += WallAvoidance();

	///Obstacle Avoidance
	m_SteeringForce += ObstacleAvoidance();

	///Arrive
	m_SteeringForce += Arrive(target);

	///Entity Avoidance
	m_SteeringForce += EntityAvoidance();

	m_SteeringForce.y = 0;

	return *D3DXVec3Normalize(&m_SteeringForce, &(m_SteeringForce * (m_WallWeight + m_ObstacleWeight + m_ArriveWeight)));
	// /(m_WallWeight + m_ObstacleWeight + m_ArriveWeight);
}

D3DXVECTOR3 SteeringManager::Pursue(Entity* Target)
{
	///If the object is looking at us then we can just seek them out..
	D3DXVECTOR3 toTarget = *D3DXVec3Normalize(&toTarget, &(Target->GetPosition() - SBase->GetPosition()));

	///Dotting the Heading of the objects
	FLOAT relativeDirection = D3DXVec3Dot(&SBase->GetVelocity(), &Target->GetVelocity());

	///acos(.95) = 18 deg our fov when chasing out the target
	if ((D3DXVec3Dot(&toTarget, &SBase->GetVelocity()) > 0) && (relativeDirection < -0.95))
	{
		return Seek(Target->GetPosition());
	}

	///From here on we are saying that the object isn't ahead of us so we 
	///are going to predict were it will go...

	///look ahead time is proportional to the distance between the target 
	///and the pursuer, and is inversly proportional to the sum of the 
	///objects velocities.
	FLOAT tmag = D3DXVec3Length(&(Target->GetVelocity()));
	FLOAT lookAhead = D3DXVec3Length(&toTarget) / (SBase->AI_Base->maxSpeed + tmag);

	///Now seek the predicted future position of the target....
	return Seek(Target->GetPosition() + Target->GetVelocity() * lookAhead);
}

D3DXVECTOR3 SteeringManager::Evade(Entity* Target)
{
	D3DXVECTOR3 toTarget = *D3DXVec3Normalize(&toTarget, &(Target->GetPosition() - SBase->GetPosition()));

	FLOAT tmag = D3DXVec3Length(&Target->GetVelocity());
	FLOAT lookAhead = D3DXVec3Length(&toTarget) / (SBase->AI_Base->maxSpeed + tmag);

	return Flee(Target->GetPosition() + Target->GetVelocity() * lookAhead);
}

//D3DXVECTOR3 SteeringManager::WallAvoidance() const
//{
//	D3DXVECTOR3 WForce;
//
//	double distancetoWall = 0.0;
//	double distancetoClstWall = 999999;
//
//	//index for the walls
//	int closestWall = -1;
//
//	D3DXVECTOR3 point = D3DXVECTOR3(0, 0, 0);
//	D3DXVECTOR3 closestPoint = D3DXVECTOR3(0, 0, 0);
//	D3DXVECTOR3 poc = D3DXVECTOR3(0, 0, 0);
//
//	///examine the feelers, and run through each wall
//	for (auto f = SBase->m_WD.begin(); f != SBase->m_WD.end(); f++)
//	{
//		for (int w = 0; w != WMI->getPlaneList().size(); w++)
//		{
//			if (WMI->getPlaneList()[w]->getPlaneA().getX() > 0
//				&& f->getX() //SBase->Base->getPosition().getX() + SBase->Base->getHalfExtentsX() * 1.4
//			> WMI->getPlaneList()[w]->getPlaneA().getX() - WMI->getPlaneList()[w]->GetOffset())
//			{
//				distancetoWall = (SBase->Base->getPosition() - WMI->getPlaneList()[w]->getPlaneA()).magnitude();
//
//				if (distancetoWall < distancetoClstWall)
//				{
//					distancetoClstWall = distancetoWall;
//
//					closestWall = w;
//
//					closestPoint.setX(SBase->Base->getPosition().getX() + (distancetoWall
//						- (SBase->Base->getHalfExtentsX() + WMI->getPlaneList()[w]->GetOffset())));
//					closestPoint.setY(0);
//					closestPoint.setZ(SBase->Base->getPosition().getZ() - (distancetoWall
//						- (SBase->Base->getHalfExtentsX() + WMI->getPlaneList()[w]->GetOffset())));
//
//					poc += closestPoint;
//				}
//			}
//			if (WMI->getPlaneList()[w]->getPlaneA().getX() < 0
//				&& f->getX() //SBase->Base->getPosition().getX() - SBase->Base->getHalfExtentsX() * 1.4
//				< WMI->getPlaneList()[w]->getPlaneA().getX() + WMI->getPlaneList()[w]->GetOffset())
//			{
//				distancetoWall = (SBase->Base->getPosition() - WMI->getPlaneList()[w]->getPlaneA()).magnitude();
//
//				if (distancetoWall < distancetoClstWall)
//				{
//					distancetoClstWall = distancetoWall;
//
//					closestWall = w;
//
//					closestPoint.setX(SBase->Base->getPosition().getX() - (distancetoWall
//						- (SBase->Base->getHalfExtentsX() + WMI->getPlaneList()[w]->GetOffset())));
//					closestPoint.setY(0);
//					closestPoint.setZ(SBase->Base->getPosition().getZ() + (distancetoWall
//						- (SBase->Base->getHalfExtentsX() + WMI->getPlaneList()[w]->GetOffset())));
//
//					poc += closestPoint;
//				}
//			}
//			if (WMI->getPlaneList()[w]->getPlaneA().getZ() > 0
//				&& f->getZ() //SBase->Base->getPosition().getZ() + SBase->Base->getHalfExtentsZ() * 1.4
//				> WMI->getPlaneList()[w]->getPlaneA().getZ() - WMI->getPlaneList()[w]->GetOffset())
//			{
//				distancetoWall = (SBase->Base->getPosition() - WMI->getPlaneList()[w]->getPlaneA()).magnitude();
//
//				if (distancetoWall < distancetoClstWall)
//				{
//					distancetoClstWall = distancetoWall;
//
//					closestWall = w;
//
//					closestPoint.setX(SBase->Base->getPosition().getX() - (distancetoWall
//						- (SBase->Base->getHalfExtentsX() + WMI->getPlaneList()[w]->GetOffset())));
//					closestPoint.setY(0);
//					closestPoint.setZ(SBase->Base->getPosition().getZ() + (distancetoWall
//						- (SBase->Base->getHalfExtentsX() + WMI->getPlaneList()[w]->GetOffset())));
//
//					poc += closestPoint;
//				}
//			}
//			if (WMI->getPlaneList()[w]->getPlaneA().getZ() < 0
//				&& f->getZ() //SBase->Base->getPosition().getZ() - SBase->Base->getHalfExtentsX() * 1.4
//				< WMI->getPlaneList()[w]->getPlaneA().getZ() + WMI->getPlaneList()[w]->GetOffset())
//			{
//				distancetoWall = (SBase->Base->getPosition() - WMI->getPlaneList()[w]->getPlaneA()).magnitude();
//
//				if (distancetoWall < distancetoClstWall)
//				{
//					distancetoClstWall = distancetoWall;
//
//					closestWall = w;
//
//					closestPoint.setX(SBase->Base->getPosition().getX() + (distancetoWall
//						- (SBase->Base->getHalfExtentsX() + WMI->getPlaneList()[w]->GetOffset())));
//					closestPoint.setY(0);
//					closestPoint.setZ(SBase->Base->getPosition().getZ() - (distancetoWall
//						- (SBase->Base->getHalfExtentsX() + WMI->getPlaneList()[w]->GetOffset())));
//
//					poc += closestPoint;
//				}
//			}
//		}///next wall
//	}
//
//	///if intersection has been detected, calculate a force that will push it away
//	if (closestWall >= 0)
//	{
//		//calculate by what distance the projected position of the Entity will over shoot
//		D3DXVECTOR3 overshoot = (SBase->Base->getPosition() + (SBase->Base->getVelocity().normalize()) * 1.4)
//			- poc;
//
//		//create a force in the direction of the wall, with a magnitude of the overshoot
//		WForce = (WMI->getPlaneList()[closestWall]->getNormal() * overshoot.magnitude() / SBase->GetMSpeed());
//	}
//
//	return WForce;
//}

D3DXVECTOR3 SteeringManager::ObstacleAvoidance() const
{
	D3DXVECTOR3 OForce;
	Entity		best;

	double maxRange = 99999;

	for (auto i = KRNL->GetEntities().begin(); i != KRNL->GetEntities().end(); i++)
	{
		double distance = abs(D3DXVec3Length(&(SBase->GetPosition() - (*i)->GetPosition())));

			if (distance < maxRange)
			{
				maxRange = distance;
				best = *(*i);
				continue;
			}
	}

	bool result = Test(&best);

	if (result == true)
	{
		///get penetration
		D3DXVECTOR3 n1, n2, pen;
		
		n1 = (SBase->GetPosition() - best.GetPosition()); 
		n2 = *D3DXVec3Normalize(&n2, &(SBase->GetPosition() - best.GetPosition())) * SBase->AI_Base->maxSpeed;
		pen = n1 - n2;							///vector amount displaying how much space there is between the entity and the obstacle

		pen.y = 0;

		//SBase->Base->GetPosition() += pen;

		OForce = *D3DXVec3Normalize(&OForce, &(SBase->GetVelocity() + pen));
		//OForce = ((SBase->Base->getVelocity()).normalize() * -1) + (pen);// * 4);

		//OForce = OForce.normalize();

		return OForce;
	}

	return OForce;
}

bool SteeringManager::Test(Entity* e) const
{
	int		max_distance	= 99999;
	bool	option			= false;

	for (auto i = SBase->AI_Base->getAIEnemies().begin(); i != SBase->AI_Base->getAIEnemies().end(); i++)
	{
		if ((*i) == e->AI_Base)
		{
			double distance = abs(D3DXVec3Length(&(SBase->GetPosition() - e->GetPosition())));

			if (distance < max_distance)
			{
				option = true;
			}
		}
		else
			continue;
	}

	return option;
}

D3DXVECTOR3 SteeringManager::EntityAvoidance()
{
	D3DXVECTOR3 EForce;
	Entity		best;

	int maxRange = 99999;

	for (auto i = KRNL->GetEntities().begin() ; i != KRNL->GetEntities().end(); i++)
	{
		if ((*i)->AI_Base != NULL)
		{
			FLOAT enemyIMASS = 1 / (*i)->rigidbody->Mass();
			if (enemyIMASS > 0.0f)
			{
				if (D3DXVec3Length(&((*i)->GetPosition() - SBase->GetPosition())) < maxRange)
				{
					maxRange = (int)D3DXVec3Length(&((*i)->GetPosition() - SBase->GetPosition()));
					best = *(*i);
					continue;
				}
			}
		}
	}

	bool result = Test(&best);

	if (result == true)
	{
		EForce = Evade(&best);

		*D3DXVec3Normalize(&EForce, &EForce);

		return EForce;
	}

	return EForce;
}

//D3DXVECTOR3 SteeringManager::PointToWorld(D3DXVECTOR3 world)
//{
//	SBase->Base->getPointInWorldSpace(world);
//
//	return world;
//}
//
//D3DXVECTOR3 SteeringManager::PointToLocal(D3DXVECTOR3 local)
//{
//	SBase->Base->getPointInLocalSpace(local);
//
//	return local;
//}
//
//void SteeringManager::CreateFeeler() const
//{
//	/*D3DXVECTOR3 direction = D3DXVECTOR3(0, 0, 0);
//
//	///get the direction we are heading in
//	direction = SBase->Base->getVelocity().normalize();
//
//	///setup points
//	SBase->m_WD = (SBase->Base->getPosition() + direction) * 1.4;*/
//
//	SBase->m_WD.clear();
//	vector<D3DXVECTOR3>().swap(SBase->m_WD);
//
//	D3DXVECTOR3 direction = D3DXVECTOR3(0, 0, 0);
//	int xt = 0;
//	int zt = 0;
//
//	if (SBase->Base->getPosition().getX() > 0)
//		xt = 1;
//	else
//		xt = -1;
//
//	if (SBase->Base->getPosition().getZ() > 0)
//		zt = 1;
//	else
//		zt = -1;
//
//	//get direction we are heading in
//	direction = SBase->Base->getVelocity().normalize();
//
//	//setup points
//	D3DXVECTOR3 temp1 = SBase->Base->getPosition() + direction + D3DXVECTOR3(xt, 0, 0);
//	D3DXVECTOR3 temp2 = SBase->Base->getPosition() + direction + D3DXVECTOR3(0, 0, zt);
//	D3DXVECTOR3 temp3 = SBase->Base->getPosition() + direction + D3DXVECTOR3(xt / 2, 0, zt / 2);
//
//	SBase->m_WD.push_back(temp1);
//	SBase->m_WD.push_back(temp2);
//	SBase->m_WD.push_back(temp3);
//}

double SteeringManager::GetWanderRadius() const
{
	return wanderRadius;
}

double SteeringManager::GetWanderJitter() const
{
	return wanderJitter;
}

double SteeringManager::GetWanderDistance() const
{
	return wanderDistance;
}