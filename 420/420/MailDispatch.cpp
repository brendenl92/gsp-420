//MailDispatch.cpp
#include "MailDispatch.h"
#include "AIManager.h"
#include <time.h>

MailDispatch* MailDispatch::s_Instance = 0;
MailDispatch* MailDispatch::Instance()
{
	if (!s_Instance)
		s_Instance = new MailDispatch;
	return s_Instance;
}

MailDispatch::~MailDispatch()
{
}

void MailDispatch::SendToState(int r, int s, double d, int msg, void* info)
{
	AI_Mail letter(r, s, d, msg, info);				//creates a letter

	if (d <= 0) CallHandler(letter);				//sends message to obj
	else
		m_vPackage.push_back(letter);				//else throws letter back in queue
}

void MailDispatch::CalcShortestDelay(double dt)
{
	for (unsigned int i = 0; i < m_vPackage.size(); i++)
	{
		m_vPackage[i].m_Delay -= dt;				//updates the delay time on the messages in queue
		if (m_vPackage[i].m_Delay <= 0)				//if the delay is less than 0 after the update send the message off
		{
			CallHandler(m_vPackage[i]);				//sends msg to obj
			m_vPackage.erase(m_vPackage.begin() + i); //afterwards we erase the msg's spot in queue
		}
	}
}

void MailDispatch::CallHandler(const AI_Mail &letter)
{
	AIM->GetBaseAIEntity()[letter.m_Reciever]->HandleMsg(letter);
}

void MailDispatch::ShutDown()
{
	m_vPackage.erase(m_vPackage.begin(), m_vPackage.end());
	m_vPackage.clear();
	s_Instance = NULL;
}