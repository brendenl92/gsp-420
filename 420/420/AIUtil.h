/*
Created By: Bryan O'Dell
Created On: August 2nd, 2014
This is a container that will hold all of the libraries that I will
be using.*/
#pragma once

/*Added STL libraries*/
#include <algorithm>
#include <iostream>
#include <cassert>
#include <memory>
#include <string>
#include <vector>
#include <math.h>
#include <time.h>
#include <cmath>
#include <list>
#include <map>

/*Own created operator library*/
//#include "vector3.h"

/*Bit wise operators I am wanting to implement
This is for the action plan in determining the consequence
input vs. threshhold comparison.
enum options
{
	LESSTHAN		= 0x00000001,
	LESSTHANEQUALS	= 0x00000002,
	EQUALSEQUALS	= 0x00000004,
	MORETHANEQUALS	= 0x00000008,
	MORETHAN		= 0x00000010
};

This will help in determining just what type of AI bot 
we will be having.
enum type
{
	AIBOTS			= 0x00000020,
	NPCS			= 0x00000040
};

This will be used in determining a robust state machine
system where we could create multiple action plans, and
multiple states, while allowing us to selectively apply
certain states to some and other states to others.
I.E. we wouldn't want the enemy NPC to have access to a
regular NPC's FOLLOWNODETRACK state. This state allows
the regular NPC to follow a predefined node track around 
a specific area. If we gave this to a enemy NPC they would 
never be able to reach dynamic areas, plus the predicability
of the Enemy would be easy to read within a few encounters.
For now our states to choose from are.
enum state_options
{
	NONE			= 0x00000080,
	ATTACK			= 0x00000100,
	FLEE			= 0x00000200,
	PURSUE			= 0x00000400,
	EVADE			= 0x00000800,
	FOLLOWNODETRACK = 0x00001000
};

When testing for my zone I will be creating a map as to determine
just how much of a threat a entity will be torwards another entity
this map will consist of these variables:
map<BaseAIEntity*, double distance, char threat>

The threats will be created like so:
enum threat
{
	HARMLESS,
	POTENTIALTHREAT,
	THREAT
};*/