#pragma once

#include <fstream>
#include <string>
#include <vector>
#include <Windows.h>

using namespace std;

class Log
{
private:
	ofstream OutFile;
	vector <string> logs;

public:
	Log();
	~Log();
	void log(string error);
	void send();

};

extern Log* LOG;