#pragma once
/* This is going to reperesent my Base AI Entity...
Created By: Bryan O'Dell
Created On: August 2nd, 2014

The Base AI Entity will create a pointer to ist own Finite State Machine,
Action Plan, Zone Calculations, and Steering Manager inside of the AI's
own ctor. 
*/
#pragma once
#include <iostream>
#include <algorithm>
#include <map>

#include "../inc/d3dx9.h"

#include "ActionPlan.h"
//#include "vector3.h"
#include "AISM.h"

using namespace std;

enum threat
{
	HARMLESS,
	POTENTIALTHREAT,
	THREAT
};

//Forward Declarations
class SteeringManager;
class Entity;

class BaseAIEntity
{
public:
	BaseAIEntity();
	BaseAIEntity(char type, Entity* e);
	~BaseAIEntity();

public:
	void		Zone(Entity* e);
	void		Update(float dt, Entity* e);
	void		Shutdown();
	void		HandleMsg(AI_Mail message);
	D3DXVECTOR3	Truncate(D3DXVECTOR3 s, float ms);

public:
	/*Getters*/
	vector<BaseAIEntity*>	getAIEnemies();

private:
	static int				counter;
	vector<BaseAIEntity*>	m_Enemies;
	double					i_Threshhold;	//Inner Threshhold
	double					o_Threshhold;	//Outter Threshhold

private:
	//map<BaseAIEntity*, double, int>	m_EPrior;

public:
	int						hp;				//AI health
	int						c_ID;			//Closest Enemy ID to AI
	int						ai_ID;			//AI ID
	int						cs_ID;			//Current State ID
	char					type;			//AI Type
	AISM*					m_asm;			//AI State Machine
	float					maxSpeed;		//AI Max Speed
	//vector3				m_Position;		//AI Position
	//vector3				m_Velocity;		//AI Velocity
	ActionPlan*				m_MyPlan;		//AI Action Plan
	SteeringManager*		p_mSteering;	//AI Steerign Manager

};
