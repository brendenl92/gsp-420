#pragma once
#include <vector>
enum messages
{
	
	SUCCESS = 0x01,
	FAIL = 0x02,
	PLAYSOUND = 0x04,
	SHUTDOWN = 0x08

};

enum cores
{

	AUDIO = 0x01,
	KERNEL = 0x02,

};

enum priority
{
	HIGH = 0x01,
	MEDIUM = 0x02,
	LOW = 0x04
};

class Mail
{
public:
	char sender, target, message, priority;
	void* data;
	Mail();
	Mail(char S, char T, char M, char P, void* d);
	~Mail();
};

