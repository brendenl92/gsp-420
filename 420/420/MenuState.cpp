#include "MenuState.h"
#include "GUICore.h"
#include "Input.h"

MenuState::MenuState()
{

}

MenuState::~MenuState()
{

}

void MenuState::Initialize()
{
	GUI->StartUp();
}

void MenuState::UpdateScene(float dt)
{
	GUI->Draw();
}

void MenuState::HandleInput(CMD* raw)
{
	if (raw->cmd == 125)
	{
		char* IsMouseDown = (char*)raw->extra;

		if (IsMouseDown == (char*)1)
			GUI->isMouseDown(IsMouseDown);

		if (IsMouseDown == (char*)0)
			GUI->isMouseDown(IsMouseDown);
	}

	if (raw->cmd == 126)
	{
		D3DXVECTOR4* MousePoint = (D3DXVECTOR4*)raw->extra;
		GUI->SetMousePos(MousePoint);
	}
}

void MenuState::Leave()
{
	GUI->ShutDown();
}