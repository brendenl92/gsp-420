#include "Wander.h"
#include "AIManager.h"

Wander::Wander()
: BaseState()
{
	BaseName = "Wander";
}

Wander::Wander(AISM* f, int m_ID)
: BaseState(f)
{
	m_AISM		= f;
	m_ownerID	= m_ID;
	BaseName	= "Wander";
}

Wander::~Wander()
{
	m_ownerID = NULL;
}

void Wander::Enter()
{

}

void Wander::Execute(double dt)
{

}

void Wander::Exit()
{

}

void Wander::HandleMsg(AI_Mail message)
{
	switch (message.m_Msg)
	{
	case _OnPatrol:
	{
					  ///On Patrol....
					  AIM->GetBaseAIEntity()[message.m_Reciever]->cs_ID = 1;
					  break;
	}
	case _LockedOn:
	{
					  ///Locked On....
					  AIM->GetBaseAIEntity()[message.m_Reciever]->cs_ID = 2;
					  m_AISM->Transition("Attack");
					  break;
	}
	case _FallingBack:
	{
					  ///Falling Back....
					  AIM->GetBaseAIEntity()[message.m_Reciever]->cs_ID = 3;
					  m_AISM->Transition("Flee");
					  break;
	}
	default:
					  break;
	}
}
