#pragma once
#include "RigidBody.h"

struct BoundingVolume;



enum Shape_Type{VSPHERE, VCUBE};

////////////////////////////////
//Bounding Volume Node
///////////////////////////////
class BoundingVolumeNode
{
public:

	BoundingVolumeNode(Entity* Body, BoundingVolume* Volume);

	~BoundingVolumeNode();

	Entity* mBody;

	BoundingVolume* mVolume;

};



////////////////////////////
//Bounding Volume
//////////////////////////
struct BoundingVolume
{
	int mShape;
	float mRadius;
	D3DXVECTOR3 mHalfsize;

	BoundingVolume(float Radius, D3DXVECTOR3 Halfsize = D3DXVECTOR3(0, 0, 0));
	//BoundingVolume(float Radius, UINT environment, V3 Halfsize = V3(0, 0, 0));
};