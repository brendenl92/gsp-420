#include "Contact.h"
#include "collision function.h"
#include "Entity.h"


//#include "MasterEntity.h"

//////////////////
//Contact
/////////////////
Contact::Contact(BoundingVolumeNode* lhs, BoundingVolumeNode* rhs, V3 POC, V3 Normal, float Depth, float Epsilon)
{
	mVolumes[0] = lhs;
	mVolumes[1] = rhs;
	mPoint = POC;
	mNormal = Normal;
	mDepth = Depth;
	mEpsilon = Epsilon;
}


CollisionData* CollisionData::Instance()
{
	static CollisionData* cd = new CollisionData(20, 20);
	return cd;
}

/////////////////////
//Collision Data
////////////////////
CollisionData::CollisionData(int MC, int MI)
{
	mMaxContacts = MC;
	mMaxItterations = MC*4;
}

void CollisionData::AddContact(Contact* contact)
{
	int i = mContacts.size();

	if (i >= mMaxContacts)
		return;
	else
		mContacts.push_back(contact);

	//transform into priority queue logic
}


//Detector
bool CollisionData::DetectCollision(BoundingVolumeNode* lhs, BoundingVolumeNode* rhs)
{
	return mDetector.DetectCollisions(lhs, rhs);
}


//Generate Contact
Contact* CollisionData::GenerateContact(BoundingVolumeNode* lhs, BoundingVolumeNode* rhs)
{
	Contact* C = new Contact(*mGenerator.GenerateContact(lhs, rhs));
	AddContact(C);
	return C;
}

//Resolve Contacts
void CollisionData::RsolveContacts()
{
	int i = 0;

	if (mContacts.size() > 0)
	{
		while (i < mMaxItterations)
		{
 			mResolver.InterpenetrationFix(mContacts[i]);
			delete mContacts[i];
			mContacts[i] = NULL;
			mContacts.erase(mContacts.begin() + i);
			i++;

			if (((unsigned)i) >= mContacts.size())
				return;
		}
	}
}



/////////////////////
//Collision Detector
/////////////////////
bool CollisionDetector::DetectCollisions(BoundingVolumeNode* lhs, BoundingVolumeNode* rhs)
{
	//int priotity = Priority(lhs, rhs);	//Adjust priority of order of volumes to test
										//in order to reduce the number of tests required
	bool option = false;

	if (lhs->mVolume->mShape == VSPHERE &&
		rhs->mVolume->mShape == VSPHERE)
		option = SphereToSphere(lhs, rhs);
	else
		option = false;

	return option;
}

bool CollisionDetector::SphereToSphere(BoundingVolumeNode* lhs, BoundingVolumeNode* rhs)
{
	//test collision
	V3 p1, p2;
	float r1, r2;
	r1 = lhs->mVolume->mRadius;
	p1 = lhs->mBody->GetPosition();
	r2 = rhs->mVolume->mRadius;
	p2 = rhs->mBody->GetPosition();
	float distance = D3DXVec3LengthSq(&(p1 - p2));

	return (((r1*r1) + (r2*r2)) > distance);
}

bool CollisionDetector::SphereInsideAABB(BoundingVolumeNode* lhs, BoundingVolumeNode* rhs)
{
	//Caches
	D3DXVECTOR3 center;
	D3DXVECTOR3 Hs;
	D3DXMATRIX invtrans;
	D3DXVECTOR4 cen;
	D3DXVECTOR3 closestpoint;
	D3DXVECTOR3 P1, P2;
	float R;

	
	//Set caches

	closestpoint = D3DXVECTOR3(0, 0, 0);
	R = lhs->mVolume->mRadius;
	Hs = rhs->mVolume->mHalfsize;

	//Transform sphere center relative to box coordinates
	center = lhs->mBody->GetPosition();
	D3DXMatrixInverse(&invtrans, 0, &rhs->mBody->GetTransform());	
	D3DXVec3Transform(&cen, &center, &invtrans);

	P1.x = cen.x;
	P1.y = cen.y;
	P1.z = cen.z;

	P2 = rhs->mBody->GetPosition();


	//Clamp coordinate to box

	if (P1.x + R < P2.x + Hs.x && P1.x - R > P2.x - Hs.x)
	{
		if (P1.y + R < P2.y + Hs.y && P1.y - R > P2.y - Hs.y)
		{
			if (P1.z + R < P2.z + Hs.z && P1.z - R > P2.z - Hs.z)
			{
				return true;
			}
			else
				return false;
		}
		else
			return false;
	}
	else
		return false;

	
}



////////////////////////////////
//Contact Generator
//////////////////////////////
Contact* ContactGenerator::GenerateContact(BoundingVolumeNode* lhs, BoundingVolumeNode* rhs)
{
	if (lhs->mVolume->mShape == VSPHERE &&
		rhs->mVolume->mShape == VSPHERE)
		return SphereToSphere(lhs, rhs);
	if (lhs->mVolume->mShape == VCUBE &&
		rhs->mVolume->mShape == VCUBE)
		/*return OBBToOBB(lhs, rhs);*/
		return SphereToSphere(lhs, rhs);
	else
		return NULL;
}




//Sphere to Sphere
Contact* ContactGenerator::SphereToSphere(BoundingVolumeNode* lhs, BoundingVolumeNode* rhs)
{
	//int priority;
	V3 p1, p2, norm, point;
	float m1, m2;
	float depth;
	m1 = lhs->mBody->GetRigidBody()->Mass();
	m2 = rhs->mBody->GetRigidBody()->Mass();


	p1 = lhs->mBody->GetPosition();
	p2 = rhs->mBody->GetPosition();

	V3 midline = p1 - p2;
	float size = D3DXVec3Length(&midline);

	norm = midline * (((float)1.0) / size);
	point = p1 + midline * (float)0.5;
	depth = (lhs->mVolume->mRadius + rhs->mVolume->mRadius - size);

	return new Contact(lhs, rhs, point, norm, depth);


	////Bigger Mass takes priority
	//if (m1 < m2)
	//{
	//	p1 = rhs->mBody->GetPosition();
	//	p2 = lhs->mBody->GetPosition();
	//	priority = 1;
	//}
	//else
	//{
	//	p2 = rhs->mBody->GetPosition();
	//	p1 = lhs->mBody->GetPosition();
	//	priority = 0;
	//}


	////Contact Normal
	//norm = p2 - p1;
	//D3DXVec3Normalize(&norm, &norm);

	////Contact Point
	//if (priority)	//if rhs is hevyer
	//	point = (norm*rhs->mVolume->mRadius) + p1;	//equal to the normal of the two spheres multiplied by
	//												// the radius of the starting end of the norm vector
	//												//then adding the global position of the starting end
	//else
	//	point = (norm*lhs->mVolume->mRadius) + p1;


	////Penetration Depth
	//depth = D3DXVec3Length(&(p2 - point));


	//return new Contact(lhs, rhs, point, norm, depth);
}







////////////////////////
//Contact Resolver
///////////////////////
void ContactResolver::InterpenetrationFix(Contact* C)
{
	float dist1, dist2;
	float m1, m2, total;
	float depth;
	V3 norm, point, p1, p2;
	V3 movePerMass;
	m1 = C->mVolumes[0]->mBody->GetRigidBody()->Mass();
	m2 = C->mVolumes[1]->mBody->GetRigidBody()->Mass();


	//Cache data
	depth = C->mDepth;
	norm = C->mNormal;
	point = C->mPoint;


	//Goofy Fix
	//!!my interpenetration would be too dramatic
	depth = depth * .02f;





	//mass tests
	total = m1 + m2;

	if (total <= 0)	//Return if masses are both zero
		return;


	dist1 = dist2 = 1;	//The amount of position will change of the total depth
						//Set to 1 in case one entity has infinite mass

	//As long as the entity does not have infinite mass
	//Set the amount the entity will move of the total depth
	if (m1 > 0)
		dist1 = m2 * 100 / total / 100 * depth;
	if (m2 > 0)
		dist2 = m1 * 100 / total / 100 * depth;

	
	//	movePerMass = norm * (depth / total);

	/*if (m1 > 0)
		dist1 = (m2 / total) * depth * norm;
	if (m2 > 0)
		dist2 = m1 * 100 / total / 100 * depth;*/

	/*if (m1 > 0)
		p1 = movePerMass * m1;
	if (m2 > 0)
		p2 = movePerMass * -m2;*/

	//if (D3DXVec3Length(&C->mVolumes[0]->mBody->Velocity()) <= 0)
	//{
	//	dist2 = 1;
	//	dist1 = 0;
	//}

	//if (D3DXVec3Length(&C->mVolumes[1]->mBody->Velocity()) <= 0)
	//{
	//	dist1 = 1;
	//	dist2 = 0;
	//}


	//Calculate movement along the normal
	//then set the positions
	//Zero out velocity to stop continus contact generation
	float dicks = C->mVolumes[0]->mBody->GetPosition().y;
	float dicks2 = C->mVolumes[1]->mBody->GetPosition().y;
	p1 = (C->mVolumes[0]->mBody->GetPosition() + norm * dist1);
	p2 = (C->mVolumes[1]->mBody->GetPosition() + norm * -1 * dist2);
	p1.y = dicks;
	p2.y = dicks2;
	C->mVolumes[0]->mBody->SetPosition(p1);
	//C->mVolumes[0]->mBody->SetVelocity(V3(0, 0, 0));
	C->mVolumes[1]->mBody->SetPosition(p2);
	//C->mVolumes[1]->mBody->SetVelocity(V3(0, 0, 0));
}