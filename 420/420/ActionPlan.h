#pragma once

#include "ActionNode.h"

class ActionPlan
{
private:
	vector<ActionNode*> m_Plan;

public:
	ActionPlan();
	~ActionPlan();

public:
	void AddNode(ActionNode an);
	int  Consult();

public:
	vector<ActionNode*> GetNode();
};

