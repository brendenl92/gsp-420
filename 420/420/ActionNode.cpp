#include "ActionNode.h"


ActionNode::ActionNode(int i_State, ActionTerm* at, int e_State)
{
	input_state		= i_State;
	m_AT			= at;
	m_AA			= NULL;
	m_OA			= NULL;
	end_state		= e_State;
}

ActionNode::ActionNode(int i_State, AndAction* aa, int e_State)
{
	input_state = i_State;
	m_AT		= NULL;
	m_AA		= aa;
	m_OA		= NULL;
	end_state = e_State;
}

ActionNode::ActionNode(int i_State, OrAction* oa, int e_State)
{
	input_state = i_State;
	m_AT		= NULL;
	m_AA		= NULL;
	m_OA		= oa;
	end_state = e_State;
}

 bool  ActionNode::Test()
{
	if (m_AT != NULL)
		return m_AT->Test();
	else if (m_AA != NULL)
		return m_AA->Test();
	else if (m_OA != NULL)
		return m_OA->Test();
	else
		return false;
	return false;
}

int ActionNode::GetInputState()
{
	return input_state;
}

int ActionNode::GetEndState()
{
	return end_state;
}
