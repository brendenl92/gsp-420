#pragma once

#include "ActionSet.h"
#include <vector>

class ActionTerm
{
private:
	ActionSet m_AS;

public:
	ActionTerm(int i, int t, char c_O);
	ActionTerm(ActionTerm& m_AT);
	ActionTerm()  {}
	~ActionTerm() {}

public:
	bool Test();
	ActionSet			GetSet();

};

class AndAction : ActionTerm
{
private:
	vector<ActionTerm*> m_Pair;

public:
	AndAction(ActionTerm& m_AT1, ActionTerm& m_AT2)
	{
		m_Pair.push_back(&m_AT1);
		m_Pair.push_back(&m_AT2);
	}

	AndAction(){};

	~AndAction()
	{
		vector<ActionTerm*> temp;
		temp.swap(m_Pair);
		m_Pair.clear();
	}

public:
	bool Test();
};

class OrAction : ActionTerm
{
private:
	vector<ActionTerm*> m_Pair;

public:
	OrAction(ActionTerm& m_AT1, ActionTerm& m_AT2)
	{
		m_Pair.push_back(&m_AT1);
		m_Pair.push_back(&m_AT2);
	}

	OrAction(){};

	~OrAction()
	{
		vector<ActionTerm*> temp;
		temp.swap(m_Pair);
		m_Pair.clear();
	}

public:
	bool Test();
};