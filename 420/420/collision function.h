#pragma once

#include <d3d9.h>
#include "../inc/d3dx9.h"

//#include "DXUtil.h"

class Contact;
class BoundingVolumeNode;

D3DXVECTOR3 Cross(D3DXVECTOR3& Va, D3DXVECTOR3& Vb);

float GetM(D3DXVECTOR3 V, int dim);

void SetM(D3DXVECTOR3& V, int dim, float value);

//Invert Z Axis
void InvertZAxis(D3DXMATRIX& RotM);

static inline D3DXVECTOR3 contactPoint(D3DXVECTOR3 &pOne, D3DXVECTOR3 &dOne, float oneSize, D3DXVECTOR3 &pTwo, D3DXVECTOR3 &dTwo, float twoSize,
	// If this is true, and the contact point is outside
	// the edge (in the case of an edge-face contact) then
	// we use one's midpoint, otherwise we use two's.
	bool useOne)
{
	D3DXVECTOR3 toSt, cOne, cTwo;
	float dpStaOne, dpStaTwo, dpOneTwo, smOne, smTwo;
	float denom, mua, mub;

	smOne = D3DXVec3LengthSq(&dOne);
	smTwo = D3DXVec3LengthSq(&dTwo);
	dpOneTwo = D3DXVec3Dot(&dTwo, &dOne);

	toSt = pOne - pTwo;
	dpStaOne = D3DXVec3Dot(&dOne, &toSt);
	dpStaTwo = D3DXVec3Dot(&dTwo, &toSt);

	denom = smOne * smTwo - dpOneTwo * dpOneTwo;

	// Zero denominator indicates parrallel lines
	if (abs(denom) < 0.0001f) {
		return useOne ? pOne : pTwo;
	}

	mua = (dpOneTwo * dpStaTwo - smTwo * dpStaOne) / denom;
	mub = (smOne * dpStaTwo - dpOneTwo * dpStaOne) / denom;

	// If either of the edges has the nearest point out
	// of bounds, then the edges aren't crossed, we have
	// an edge-face contact. Our point is on the edge, which
	// we know from the useOne parameter.
	if (mua > oneSize ||
		mua < -oneSize ||
		mub > twoSize ||
		mub < -twoSize)
	{
		return useOne ? pOne : pTwo;
	}
	else
	{
		cOne = pOne + dOne * mua;
		cTwo = pTwo + dTwo * mub;

		return cOne * 0.5 + cTwo * 0.5;
	}
}

//Get Axis of rotation matrix
D3DXVECTOR3 GetAxisR(D3DXMATRIX& RotM, int Col);

//Create orthonormal basis based on a forward vector
void OrthnormBasFwdVec(D3DXMATRIX& RotM, D3DXVECTOR3& Heading);
