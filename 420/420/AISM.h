/*
Created By: Bryan O'Dell
Created On: August 2nd, 2014

This will show how a base state should be setup and how the 
fsm will interact with the states themselves, and how to add
and removes states from the fsm. The fsm itself is setup to 
be dynamically created by each AI so they can "all" have 
"their" own fsm at hand.
*/
#pragma once

#include <iostream>
#include <string>
#include <list>
#include <memory>
#include <cassert>
#include "MailDispatch.h"

using namespace std;

///forward declaration
class AISM;

///this class will represent my base state
class BaseState
{
public:
	BaseState();
	BaseState(int m_ID);						//ctor
	BaseState(AISM* f) { m_AISM = f; }
	virtual ~BaseState();						//dctor

public:
	virtual void Enter() = 0;					//enter
	virtual void Execute(double dt) = 0;		//execute
	virtual void Exit() = 0;					//exit
	virtual void HandleMsg(AI_Mail message) = 0;//handles the message passed in

public:
	string BaseName;							//used to switch between states
	AISM* m_AISM;

protected:
	int m_ownerID;								//stores obj's ID
};

//will create a list of shared pointers housing all the states in the machine
typedef list< shared_ptr<BaseState> > StateList;

//Finite State Machine
class AISM
{
public:
	AISM();										//ctor
	~AISM();

public:
	void Update(double dt);
	void HandleMsg(AI_Mail message);
	void Transition(string BaseName);
	void Delay(string BaseName);
	void AddState(BaseState *newState, bool makeCurrent);
	string GetState();

public:
	BaseState*	currentState;
	StateList	statelist;
	string		DelayState;
};