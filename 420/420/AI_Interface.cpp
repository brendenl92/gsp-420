#include "AI_Interface.h"
#include "Entity.h"
AI_Interface* AI_Interface::s_Instance = 0;
AI_Interface* AI_Interface::Instance()
{
	if (!s_Instance)
		s_Instance = new AI_Interface;
	return s_Instance;
}

AI_Interface::AI_Interface()
{
}


void AI_Interface::Init()
{
	//Add stuff here later
}

BaseAIEntity* AI_Interface::CreateAIEntity(int type, Entity* e)
{
	if (type = 0)
		return AIM->CreateAIEntity(AIBOTS, e);
	else if (type = 1)
		return AIM->CreateAIEntity(NPCS, e);
	return AIM->CreateAIEntity(type, e);
}

void AI_Interface::Shutdown()
{
	AIM->Shutdown();
}

void AI_Interface::Update(float dt, Entity* e)
{
	AIM->GetBaseAIEntity()[e->AI_Base->ai_ID]->Update(dt, e);
}