/*
Created By: Bryan O'Dell
Created On: August 2nd, 2014
This is the AI Manager, this will be the quintessential
class for the AI core. Here this class will be utilized 
in being the bridge between the front end interface and 
the back end program. This will also be the AI "HUB" of 
sorts where the AI Entities will be stored & created, 
where the user will actually be able to add more data
action plans to specific AI Entities through the 
interface. */
#pragma once

#include "BaseAIEntity.h"

#define AIM AIManager::Instance()

enum AI_type
{
	AIBOTS			= 0x00000020,
	NPCS			= 0x00000040
};

class AIManager
{
public:
	AIManager() {}
	~AIManager() {}

public:
	static AIManager* Instance();

public:
	void					Init();
	BaseAIEntity*			CreateAIEntity(char AI_type, Entity* e);
	/*DON'T USE ATM
	void					AddPlan(int ai_ID, int predicator_ID, int result_ID);
	int						Consequence(double input, double threshhold, enum option);
	*/
	void					Shutdown();
	vector<BaseAIEntity*>	GetBaseAIEntity();

private:
	static AIManager*		s_Instance;
	static int				s_cID;				//Consequence ID
private:
	vector<BaseAIEntity*>	m_vAIEntities;

};

