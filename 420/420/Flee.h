#pragma once
#include "AISM.h"
class Flee :
	public BaseState
{
public:
	Flee();
	Flee(AISM* f, int m_ID);
	~Flee();								//dctor

public:
	void Enter();							//enter
	void Execute(double dt);				//execute
	void Exit();							//exit
	void HandleMsg(AI_Mail message);		//handles the message passed in

public:
	string BaseName;						//used to switch between states
	AISM* m_AISM;

protected:
	int m_ownerID;							//stores obj's ID
};

