#pragma once

#include <iostream>

using namespace std;

enum Compare
{
	LESSTHAN,
	LESSTHANEQUALS,
	EQUALSEQUALS,
	MORETHANEQUALS,
	MORETHAN
};

class ActionSet
{
private:
	int		input;
	int		threshold;
	char	comOption;

public:
	ActionSet(int i, int t, char c_O);
	ActionSet() {}
	~ActionSet(){}

public:
	bool IsMet();
	
};

