#include "Entity.h"
#include "PhysicsInterface.h"

Entity::Entity(void)
{
	mRigidEnabled = false;
	D3DXQuaternionIdentity(&orientation);
	position = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	velocity = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
}


Entity::~Entity(void)
{
	//Individual AI Shutdown
	if (AI_Base != NULL)
	{
		AI_Base->Shutdown();
		AI_Base = NULL;
	}

	//Individual Rigid Body Shutdown
	if (rigidbody != NULL)
	{
		if (rigidbody->mVolume != NULL)
			PhyI->RemoveCollisionVolume(this);

		delete rigidbody;
		rigidbody = NULL;
	}
}

void Entity::addComponent(BaseComponent* addition)
{
	components.push_back(addition);
}

void Entity::CreateComponent(char type, char AI_type, FLOAT m, FLOAT damp, int shape, D3DXVECTOR3 h_size, D3DXVECTOR3 pos, D3DXVECTOR3 vel)
{
	position = pos;
	velocity = vel;

	if (type & AIENABLE)
	{
		AI_Base = AIF->CreateAIEntity(AI_type, this);
	}
	if (type & PHYSICSENABLE)
	{
		AddRigidBody(m, shape, h_size, damp);
	}
	if (type & GRAPHICSENABLE)
	{
		///Everything is going to be drawn as floating dicks...
	}
}

void Entity::Update(float dt)
{
	if (AI_Base != NULL)
	{
		AIF->Update(dt, this);
	}

	if (rigidbody != NULL)
	{
		UpdateRigidBody();
	}

	for (int i = 0; i < components.size(); i++)
		components[i]->update(this);
}

void Entity::AddRigidBody(float  mass, int shape, D3DXVECTOR3 halfsize, float dampening)
{
	//Create Rigid Body
	rigidbody = new RigidBody(mass, shape, halfsize, dampening);
	//components.push_back(rigidbody);
	EnableRigidBody();
}

bool Entity::RigidEnabled()
{
	return mRigidEnabled;
}

void Entity::UpdateRigidBody()
{
	//Test each component if it is a rigid body
	//if so update rigid body
	for (unsigned int i = 0; i < components.size(); i++)
	{
		if (components[i]->isRigidBody())
			components[i]->update(this);
	}
}

RigidBody* Entity::GetRigidBody() const
{
	return rigidbody;
}

void Entity::AddBoundingVolume(float Radius, D3DXVECTOR3 Halfsize)
{
	BoundingVolume* bv = new BoundingVolume(Radius, Halfsize);
	mBoundingVolume = new BoundingVolumeNode(this, bv);
}
