#pragma once
#include <d3dx9.h>
	
class Camera
{
	float m_TPHeight;
	float m_TPDist;
	float m_Sensitivity;

	D3DXVECTOR3 m_Position;
	D3DXVECTOR3 m_Forward;
	D3DXVECTOR3 m_Right;
	D3DXVECTOR3	m_Up;
	D3DXMATRIX m_ViewMatrix;
	D3DXMATRIX m_ProjMatrix;
	D3DXMATRIX m_ViewProjMatrix;

	void recalcVectors();
	void BuildViewProjMatrix();

public:
	Camera();
	~Camera();

	void update(float x, float y);
	void reset(D3DXVECTOR3 pos);

	void setPos(D3DXVECTOR3 position);
	void setTarget(D3DXVECTOR3 target);
	void setSensitivity(float sen);
	void adjustPos(D3DXVECTOR3 in);
	void adjustBOF(float length);
	void adjustBOR(float length);
	void adjustBOU(float length);

	D3DXVECTOR3 getPos();
	D3DXVECTOR3 getForward();
	D3DXVECTOR3 getRight();
	D3DXVECTOR3 getUp();
	D3DXMATRIX getViewProjMatrix();
};

