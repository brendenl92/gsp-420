#include "BoundingVolume.h"
//#include "RigidBody.h"

//////////////////////////////////
//Bounding Volume Node
/////////////////////////////////
//Ctor
BoundingVolumeNode::BoundingVolumeNode(Entity* Body, BoundingVolume* Volume)
{ 
	mBody = Body;
	mVolume = Volume;
}

//DCTOR
BoundingVolumeNode::~BoundingVolumeNode()
{
	delete mVolume;
}



/////////////////////////////
//Bounding Volume
///////////////////////////
BoundingVolume::BoundingVolume(float Radius, D3DXVECTOR3 Halfsize)
{
	mHalfsize = Halfsize;
	//this->environment = environment;

	//Test to see if the shape is a cube
	if (D3DXVec3LengthSq(&Halfsize) > 0)
	{
		mRadius = sqrt((Halfsize.x*Halfsize.x) + (Halfsize.y*Halfsize.y) + (Halfsize.z*Halfsize.z));
		mShape = VCUBE;
	}
	else
	{
		mRadius = Radius;
		mShape = VSPHERE;
		Halfsize.x = Radius;
		Halfsize.y = Radius;
		Halfsize.z = Radius;
	}
}