#pragma once

class Entity;

class BaseComponent
{

	unsigned int componentID;

public:
	BaseComponent(void){}; 
	~BaseComponent(void){};

	virtual void initialize(void* info) = 0;
	virtual void update(Entity* entity) = 0;
	virtual void shutdown() = 0;

	//Codi added this to test if it is a rigid body type
	virtual bool isRigidBody(){return false;}
};

