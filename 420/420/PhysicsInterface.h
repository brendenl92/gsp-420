# pragma once

#include "Contact.h"
#include <map>
#include <vector>

class Entity;

#define PhyI PhysicsInterface::Instance() 

class PhysicsInterface
{
private:
	PhysicsInterface();

	static PhysicsInterface* s_Instance;

public:

	static PhysicsInterface* Instance();

	//Variables
	
	typedef std::vector<BoundingVolumeNode*> BoundingVolumes;
	BoundingVolumes mCollisionVolumes;

	CollisionData* mCollisions;


	/*typedef std::map<RigidBody, BoundingVolumeNode*> BoundingVolumes;
	BoundingVolumes mCollisionVolumes;*/

	//Methods
	RigidBody* CreateRigidBody(Entity* Master, V3 Position, float Radius = 10.0f, V3 Halfsize = V3(0,0,0), float mass = 10.0f);
	void UpdatePhysics(float DeltaTime);
	void RemoveCollisionVolume(Entity* Body);
	void GenertateContacts();
	void ResolveContacts();

};