#pragma once

#include "State.h"
#include "MenuState.h"
#include "GameState.h"

class FSM
{
	State* m_CurrentState;
	State* m_LastState;

public:
	FSM(void);
	~FSM(void);

	void setCurrentState(State* state);
	void setLastState(State* state);

	void changeState(State* state);
	void revertStates();

	void update(float dt);

	void handleInput(CMD* cmd);

	bool isInState(State* state);

	State* getState();

};

