#include "Kernel.h"
Kernel* KRNL = nullptr;
#include "Audio.h"
#include "GUICore.h"
#include "MenuState.h"

LRESULT CALLBACK MainWndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	if (KRNL != nullptr)
		return KRNL->handleWindowsMessages(hwnd, msg, wParam, lParam);
	else
		return DefWindowProcA(hwnd, msg, wParam, lParam);
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevInstance, PSTR cmdLine, int showCmd)
{

	srand((unsigned int)time(NULL));
	KRNL = new Kernel();
	KRNL->Startup(hInstance);

}

Kernel::Kernel()
{
}


Kernel::~Kernel()
{
	delete m_FSM;
	delete Sound;
	delete Graphics;
}

void Kernel::Startup(HINSTANCE instance)
{
	m_Resolution[0] = GetSystemMetrics(SM_CXSCREEN);
	m_Resolution[1] = GetSystemMetrics(SM_CYSCREEN);

	m_Instance = instance;
	Graphics = new GraphicsCore();
	Graphics->initialize();
	
	clock = new Clock();
	clock->initialize();
	camera = new Camera();
	Sound = new Audio();
	AudioT = thread(&Audio::startup, Sound);
	input = new Input();
	settings = new Settings();
	RM = new ResourceManager();
	m_FSM = new FSM();
	m_FSM->setCurrentState(new MenuState());
	KRNL->update();
}

void Kernel::update()
{
	MSG message;
	message.message = WM_NULL;
	while (message.message != WM_QUIT)
	{
		while (PeekMessage(&message, Graphics->getHWND(), NULL, NULL, PM_REMOVE))
		{
			TranslateMessage(&message);
			DispatchMessage(&message);
		}

		Graphics->beginRender();
		if (!paused) //update entities & particle effects
		{
			m_FSM->update(.03f);
			for (UINT i = 0; i < m_Entities.size(); i++)
				m_Entities[i]->Update(.03f);
		}

		//Rendering must persist through pauses. 
		Graphics->endRender();
	}
		
}

void Kernel::forwardMail(Mail* mail)
{
	switch (mail->target)
	{
	case 1:
		Sound->addMessage(mail);
		break;
	}
}

LRESULT Kernel::handleWindowsMessages(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{ 
	switch (msg)
	{
	case WM_INPUT:
		CMD* command = input->handleMessage(msg, wParam, lParam);
		{
			if (command->cmd != 127)
			{
				m_FSM->handleInput(command);
			}
		}
		break;
	}
	return DefWindowProcA(hwnd, msg, wParam, lParam);
}


HINSTANCE Kernel::getInstance()
{
	return m_Instance;
}

void Kernel::CreateEntity(char type, char AI_type, FLOAT mass, FLOAT damp, int shape, D3DXVECTOR3 halfsize, D3DXVECTOR3 pos, D3DXVECTOR3 vel)
{
	//Creates and Entity
	Entity* e = new Entity();
	
	//Creates the components for the Entity 
	e->CreateComponent(type, AI_type, mass, damp, shape, halfsize, pos, vel);

	//Push back into vector
	m_Entities.push_back(e);
}

void Kernel::CreatePath(Entity* entity)
{
	m_Paths.push_back(entity);
	m_Entities.push_back(entity);
}

void Kernel::CreatePlayer(Entity* entity)
{
	m_Players.push_back(entity);
	m_Entities.push_back(entity);
}

void Kernel::CreateEntity(Entity* entity)
{

	m_Entities.push_back(entity);
}

void Kernel::showCursor(bool in)
{

	if (in)
	{
		int i = -100;
		do
		i = ShowCursor(TRUE);
		while (i != 0);
	}
	else
	{
		int i = 100;
		do
		i = ShowCursor(FALSE);
		while (i != -1);
	}

}

vector<Entity*> Kernel::GetEntities() const
{
	return m_Entities;
}

void Kernel::fatalError(string reason, void* extradata)
{
	Mail* mail = new Mail(KERNEL, SOUND, PLAYSOUND, HIGH, (void*)new string("../Media/sounds/nein.mp3"));
	Sound->addMessage(mail);

	PostQuitMessage(0);
}