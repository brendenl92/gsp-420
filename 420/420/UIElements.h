#pragma once
#include "../inc/d3d9.h"
#include <d3dx9.h>
#include <string>
using namespace std;
class Button
{
	int textID;
	D3DXVECTOR2 pos;
	D3DXVECTOR2 scale;
	RECT rect;
	LONG origHeight;
	LONG origWidth;
	bool chosen;
	string name;
public:
	Button();
	Button(int textID, D3DXVECTOR2 pos, string name);
	~Button(void);
	D3DXVECTOR2 getPos();
	D3DXVECTOR2 getHalfExtents();
	int getID();
	void setPos(D3DXVECTOR2 pos);
	D3DXVECTOR2 getScale();
	void setScale(D3DXVECTOR2 scale);
	string getName();
	RECT getRect();
	void update(float dt);
	void setChosen(bool derp);
	bool getChosen();

};

class Button2
{
public:
	Button2();
	Button2(int textID, D3DXVECTOR2 pos, string name);
	~Button2(void);
	D3DXVECTOR2 getPos();
	D3DXVECTOR2 getHalfExtents();
	int getID();
	void setPos(D3DXVECTOR2 pos);
	D3DXVECTOR2 getScale();
	void setScale(D3DXVECTOR2 scale);
	string getName();
	RECT getRect();
	void update(float dt);
	void setChosen(bool derp);
	bool getChosen();
	inline bool isHovering(){ return Hovering; }

private:
	int textID;
	D3DXVECTOR2 pos;
	D3DXVECTOR2 scale;
	RECT rect;
	LONG origHeight;
	LONG origWidth;
	bool chosen;
	string name;
	bool Hovering;

};

//class DropDown
//{
//	Button* m_Face;
//	vector<Button*> options;
//	int face;
//	bool clicked;
//public:
//	DropDown(vector<Button*> input);
//	~DropDown(void);
//	Button getButtons(float i);
//	string getChosenName();
//	string getOptionName();
//	void update(float dt);
//	void seek(float dt, int i);
//};

class Slider
{
	int backTextId;
	Button derp;
	D3DXVECTOR2 ends[2];
	D3DXVECTOR2 pos;
	float position;

public:
	Slider();
	Slider(Button slider, int text, D3DXVECTOR2 pos);
	~Slider(); 
	float getPosition();
	int getID();
	D3DXVECTOR2 getPos();
	void setSlidePosition(float input);
	void update(float dt);
	Button getButton();
	
};

class CheckBox
{
	int textId;
	D3DXVECTOR2 pos;
	RECT rect;
	bool click;
public:
	CheckBox(int id, D3DXVECTOR2 pos);
	~CheckBox(void);
	D3DXVECTOR2 getPos();
	D3DXVECTOR2 getHalfExtents();
	int getID();
	RECT getRect();
	void setPos(D3DXVECTOR2 pos);
	void setChecked(bool ya);
	void update(float dt);
	bool getChecked();

};

class Text
{
	string text;
	D3DXVECTOR2 pos;
	RECT rect;
	UINT font;
public:
	Text(UINT font, D3DXVECTOR2 pos, string text);
	~Text();
	D3DXVECTOR2 getPos();
	void SetPos(D3DXVECTOR2 pos, UINT font);
	RECT getRect();
	UINT getFont();
	string getText();

};

class Image
{
	D3DXVECTOR2 pos;
	D3DXVECTOR2 scale;
	UINT textID;
public:
	Image(D3DXVECTOR2 pos, D3DXVECTOR2 scale, UINT textureID); 
	~Image();
	D3DXVECTOR2 getPos();
	D3DXVECTOR2 getScale();
	UINT getTextID();
};