#pragma once
#include "PhysicsInterface.h"
#include "collision function.h"
#include "Kernel.h"


RigidBody::RigidBody(float  mass, int shape, V3 halfsize, float damp, float max_speed)
: mInvMass(mass / 1), mDampening(damp), mMaxSpeed(max_speed)
{
	InertialTensor(shape, halfsize, mass);
}

void RigidBody::update(Entity* ent)
{
	//CACHES
	V3 position;
	V3 velocity;
	float dt;


	position = ent->GetPosition();
	velocity = ent->GetVelocity();
	dt = KRNL->GetClock()->getDT();

	//If infinite mass return
	if (mInvMass <= 0.0f) return;

	//POSITION UPDATE

	//Update position
	position += velocity*dt;

	//ACCELERATION UPDATE

	//Update Linear Acceleration
//	V3 resultingAcc = ent->GetLAcceleration();
//	resultingAcc += (mForceAccum * mInvMass);

	//Update Angular Acceleration
	//V3 angleAcc = 

	//VELOCITY UPDATE

	//Update Linear velocity
//	velocity += (resultingAcc*CLK->getdt());

	//DRAG

	//Impose drag
	velocity *= pow(mDampening, dt);
	//	omega *= pow(mAngularDampening, dt);

	//SET DATA

	//set Transformation matrix with position and orientation
	ent->SetPosition(position);
	ent->SetVelocity(velocity);
	CalculateDerivedData(ent->GetTransform(), position, ent->GetOrientation());
}

void RigidBody::update(float dt, M4& trans, Qu or, V3& vel, Qu& omega, V3& Lacc)
{
	//If infinite mass return
	if (mInvMass <= 0.0f) return;

	//Update position
	V3 position = GetPosition(trans) + (vel*dt);

	SetPosition(trans, position);
	
	//Update Linear Acceleration
	V3 resultingAcc = Lacc;
	resultingAcc += (mForceAccum * mInvMass);

	//Update Angular Acceleration
	V3 angleAcc = 

	//Update Linear velocity
	vel += (resultingAcc*dt);

	//Update Linear velocity
	vel += (resultingAcc*dt);

	//Impose drag
	vel *= pow(mDampening, dt);
	omega *= pow(mAngularDampening, dt);

	//set Transformation matrix with position and orientation
	SetTransform(trans, position, or);

	//set Transformation matrix with position and orientation
	CalculateDerivedData(trans, position, or);

}

void RigidBody::CalculateDerivedData(M4& Trans, V3 pos, Qu or)
{
	//Set transform matrix
	SetTransform(Trans, pos, or);

	//Set Inverse Inertial Tensor WorldMa
	SetInvInertTensWorld(Trans, mInvInertialTensor);
}


void RigidBody::InertialTensor(int shape, V3 h, float m)
{
	switch (shape)
	{
	case VSPHERE:
		mInvInertialTensor._11 = (0.4f * m * (h.x*h.x));
		mInvInertialTensor._22 = (0.4f * m * (h.x*h.x));
		mInvInertialTensor._33 = (0.4f * m * (h.x*h.x));
		D3DXMatrixInverse(&mInvInertialTensor, 0, &mInvInertialTensor);
		break;
	case VCUBE:
		mInvInertialTensor._11 = (0.083f * m * ((h.z*h.z) + (h.y*h.y)));
		mInvInertialTensor._22 = (0.083f * m * ((h.x*h.x) + (h.y*h.y)));
		mInvInertialTensor._33 = (0.083f * m * ((h.x*h.x) + (h.z*h.z)));
		D3DXMatrixInverse(&mInvInertialTensor, 0, &mInvInertialTensor);
		break;
	}
}

void RigidBody::SetInvInertTensWorld(M4& T, M4 iit)
{
	float t4 = 
		T._11*iit._11 +
		T._12*iit._21 +
		T._13*iit._31;

	float t9 =
		T._11*iit._12 +
		T._12*iit._22 +
		T._13*iit._32;

	float t14 =
		T._11*iit._13 +
		T._12*iit._23 +
		T._13*iit._33;


	float t28 =
		T._21*iit._11 +
		T._22*iit._21 +
		T._23*iit._31;

	float t33 =
		T._21*iit._12 +
		T._22*iit._22 +
		T._23*iit._32;

	float t38 = 
		T._21*iit._13 +
		T._22*iit._23 +
		T._23*iit._33;


	float t52 = 
		T._31*iit._11 +
		T._32*iit._21 +
		T._33*iit._31;

	float t57 = 
		T._31*iit._12 +
		T._32*iit._22 +
		T._33*iit._32;

	float t62 =
		T._31*iit._13 +
		T._32*iit._23 +
		T._33*iit._33;



	mInvInertialTensorWorld._11 =
		(t4*T._11 +
		t9*T._12 +
		t14*T._13);

	mInvInertialTensorWorld._12 =
		t4*T._21 +
		t9*T._22 +
		t14*T._23;

	mInvInertialTensorWorld._13 =
		t4*T._31 +
		t9*T._32 +
		t14*T._33;


	mInvInertialTensorWorld._21 =
		t28*T._11 +
		t33*T._12 +
		t38*T._13;

	mInvInertialTensorWorld._22 =
		t28*T._21 +
		t33*T._22 +
		t38*T._23;

	mInvInertialTensorWorld._23 =
		t28*T._31 +
		t33*T._32 +
		t38*T._33;


	mInvInertialTensorWorld._31 =
		t52*T._11 +
		t57*T._12 +
		t62*T._13;

	mInvInertialTensorWorld._32 =
		t52*T._21 +
		t57*T._22 +
		t62*T._23;

	mInvInertialTensorWorld._33 =
		t52*T._31 +
		t57*T._32 +
		t62*T._33;
}


void RigidBody::AddForce(V3 f)
{
	mForceAccum += f;
	mAwake = true;
}

void RigidBody::AddTorqueAtPoint(M4& trans, V3& force, V3& point)
{
	
	//Convert to coordinates relative to center of mass
	V3 pt = point;
	pt -= GetPosition(trans);

	mForceAccum += force;
	D3DXVec3Cross(&mTorque, &pt, &force);

	mAwake = true;
}

void RigidBody::AddTorqueAtBodyPoint(M4& trans, V3& force, V3& point)
{
	//Convert to coordinates relative to center of mass
	V3 pt = GetPointInWorldSpace(trans, point);
	AddTorqueAtPoint(trans, force, pt);
}

void RigidBody::ClearAccumulator()
{
	mForceAccum.x = mForceAccum.y = mForceAccum.z = 0;
	mTorque.x = mTorque.y = mTorque.z = 0;
}

V3 RigidBody::GetPointInWorldSpace(M4& Trans, V3 point)
{
	D3DXVECTOR4 v4;
	D3DXVec3Transform(&v4, &point, &Trans);
	point.x = v4.x; point.y = v4.y; point.z = v4.z;
	return point;
}

V3 RigidBody::GetPosition(M4 T)
{
	V3 p;
	p.x = T._13; p.y = T._23; p.z = T._33;
	return p;
}


void RigidBody::SetPosition(M4& t, V3 p)
{
	t._13 = p.x;
	t._23 = p.y;
	t._33 = p.z;
}

void RigidBody::SetTransform(M4& trans, V3 pos, Qu or)
{
	D3DXMatrixTransformation(&trans, &pos, 0, 0, 0, &or, 0);
}

bool RigidBody::isRigidBody()
{
	return true;
}

//Mass
float RigidBody::Mass()
{
	if (mInvMass == 0)
		return 0;
	else
		return mInvMass;
}

//Get Halfsize
V3 RigidBody::GetHalfSize()
{
	return mVolume->mVolume->mHalfsize;
}
