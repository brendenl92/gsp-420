#pragma once

#include "State.h"

class MenuState : public State
{
public:
	MenuState();
	~MenuState();

	void Initialize();
	void UpdateScene(float dt);
	void HandleInput(CMD* raw);
	void Leave();
};

