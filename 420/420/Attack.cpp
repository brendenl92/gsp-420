#include "Attack.h"
#include "AIManager.h"

Attack::Attack()
: BaseState()
{
	BaseName = "Attack";
}

Attack::Attack(AISM* f, int m_ID)
: BaseState(f)
{
	m_AISM		= f;
	m_ownerID	= m_ID;
	BaseName	= "Attack";
}

Attack::~Attack()
{
	m_ownerID = NULL;
}

void Attack::Enter()
{

}

void Attack::Execute(double dt)
{

}

void Attack::Exit()
{

}

void Attack::HandleMsg(AI_Mail message)
{
	switch (message.m_Msg)
	{
	case _OnPatrol:
	{
				///On Patrol....
				AIM->GetBaseAIEntity()[message.m_Reciever]->cs_ID = 1;
				m_AISM->Transition("Wander");
				break;
	}
	case _LockedOn:
	{
				///Locked On....
				AIM->GetBaseAIEntity()[message.m_Reciever]->cs_ID = 2;
				break;
	}
	case _FallingBack:
	{
				///Falling Back....
				AIM->GetBaseAIEntity()[message.m_Reciever]->cs_ID = 3;
				m_AISM->Transition("Flee");
				break;
	}
	default:
				break;
	}
}