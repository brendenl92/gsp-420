/*
Created By: Bryan O'Dell
Created On: August 2nd, 2014

This is a Mail Dispatch, that will be in charge of handling 
messages from the states through the FSM to the AI and back
again.
*/
#pragma once

#include <vector>
#include "AI_Mail.h"

using namespace std;

#define MD MailDispatch::Instance()

class MailDispatch
{
private:
	static MailDispatch* s_Instance;

private:
	MailDispatch() {}

public:
	static MailDispatch* Instance();

public:
	~MailDispatch();

private:
	void CallHandler(const AI_Mail &letter);

public:
	void SendToState(int r, int s, double d, int msg, void* info);
	void CalcShortestDelay(double dt);
	void ShutDown();

private:
	vector<AI_Mail> m_vPackage;
};