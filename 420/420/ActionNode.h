#pragma once

#include "ActionTerm.h"

class ActionNode
{
private:
	int				input_state;
	int				end_state;
	ActionTerm*		m_AT;
	AndAction*		m_AA;
	OrAction*		m_OA;

public:
	ActionNode(int i_State, ActionTerm* at, int e_State);
	ActionNode(int i_State, AndAction* aa, int e_State);
	ActionNode(int i_State, OrAction* oa, int e_State);
	ActionNode() {};

	~ActionNode()
	{
		delete m_AT;
		delete m_AA;
		delete m_AA;

		m_AT = NULL;
		m_AA = NULL;
		m_OA = NULL;
	}

public:
	bool			Test();
	int				GetInputState();
	int				GetEndState();
};

