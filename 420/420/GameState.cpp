#include "GameState.h"
#include "input.h"
#include "Kernel.h"
#include "Contact.h"
#include "Entity.h"


GameState::GameState()
: State()
{
	fwd = bak = lft = rgt = up = dwn = mvup = mvdn = mvrt = mvlt = false;

	 StartPosition = D3DXVECTOR3(20, 0, 200);
}


GameState::~GameState()
{
}
void GameState::Initialize()
{
	KRNL->showCursor(false);
	Entity* entity = new Entity();
	TreeDeeComponent* dd = new TreeDeeComponent();
	dd->initialize(&string("../models/crate.x"));
	dd->setScale(D3DXVECTOR3(15, 3, 3.5));
	entity->addComponent(dd);
	entity->SetPosition(D3DXVECTOR3(0, 0, -8));
	Entity* entity2 = new Entity();
	TreeDeeComponent* d = new TreeDeeComponent();
	d->initialize(&string("../models/crate.x"));
	d->setScale(D3DXVECTOR3(15, 3, 3.5));
	entity2->addComponent(d);
	entity2->SetPosition(D3DXVECTOR3(0, 0, 8));
	Entity* entity3 = new Entity();
	TreeDeeComponent* ddd = new TreeDeeComponent();
	ddd->initialize(&string("../models/crate.x"));
	ddd->setScale(D3DXVECTOR3(3.5, 3, 15));
	entity3->addComponent(ddd);
	entity3->SetPosition(D3DXVECTOR3(-8, 0, 0));
	Entity* entity4 = new Entity();
	TreeDeeComponent* dddd = new TreeDeeComponent();
	dddd->initialize(&string("../models/crate.x"));
	dddd->setScale(D3DXVECTOR3(3.5, 3, 15));
	entity4->addComponent(dddd);
	entity4->SetPosition(D3DXVECTOR3(8, 0, 0));
	Entity* entity5 = new Entity();
	TreeDeeComponent* ddddd = new TreeDeeComponent();
	ddddd->initialize(&string("../models/crate.x"));
	ddddd->setScale(D3DXVECTOR3(8, 3, 10));
	entity5->SetPosition(D3DXVECTOR3(0, 0, 2));
	entity5->addComponent(ddddd);
	Entity* entity7 = new Entity();
	TreeDeeComponent* dddddd = new TreeDeeComponent();
	dddddd->initialize(&string("../models/base.x"));
	dddddd->setScale(D3DXVECTOR3(20, .001f, 20));
	entity7->SetPosition(D3DXVECTOR3(0, 0, 0));
	entity7->addComponent(dddddd);

	///Codi Made...
	KRNL->CreateEntity(entity);
	KRNL->CreateEntity(entity2);
	KRNL->CreateEntity(entity3);
	KRNL->CreateEntity(entity4);
	KRNL->CreateEntity(entity5);
	KRNL->CreateEntity(entity7);
	
	//Codi Added this

	Entity* entity6 = new Entity();
	TreeDeeComponent* something = new TreeDeeComponent();
	something->initialize(&string("../models/sphere.x"));
	something->setScale(D3DXVECTOR3(.8, .8, .8));
	entity6->addComponent(something);
	entity6->SetPosition(D3DXVECTOR3(5, 1, 5));
	entity6->AddRigidBody(10, 1, D3DXVECTOR3(1,1,1), 0.90f);
	KRNL->CreatePlayer(entity6);
	entity6->AddVelocity(D3DXVECTOR3(2.0f, 0, 0));
	player = entity6;

}


void GameState::UpdateScene(float dt)
{

	SetCursorPos(KRNL->GetGraphicsPtr()->ScreenCenter(0), KRNL->GetGraphicsPtr()->ScreenCenter(1));
	if (mvrt)
		player->SetPosition(player->GetPosition() + D3DXVECTOR3(.01, 0, 0));
	if (mvlt)
		player->SetPosition(player->GetPosition() + D3DXVECTOR3(-.01, 0, 0));
	if (mvup)
		player->SetPosition(player->GetPosition() + D3DXVECTOR3(0, 0, .01));
	if (mvdn)
		player->SetPosition(player->GetPosition() + D3DXVECTOR3(0, 0, -.01));

	if (rgt)
		KRNL->GetCameraPtr()->adjustBOR(.05f);
	if (lft)
		KRNL->GetCameraPtr()->adjustBOR(-.05f);
	if (fwd)
		KRNL->GetCameraPtr()->adjustBOF(.05f);
	if (bak)
		KRNL->GetCameraPtr()->adjustBOF(-.05f);

	if (up)
		KRNL->GetCameraPtr()->adjustBOU(.05f);
	if (dwn)
		KRNL->GetCameraPtr()->adjustBOU(-.05f);

	if (player->GetPosition().x < 5 && player->GetPosition().x > -5 && player->GetPosition().z > -3 || player->GetPosition().z < -5.5 || player->GetPosition().x < -5.5 || player->GetPosition().x > 5.5 || player->GetPosition().z > 5.5)
		player->SetPosition(D3DXVECTOR3(5, 1, 5));

	float hi = D3DXVec3Length(&(player->GetPosition() - D3DXVECTOR3(-5, 0, 3)));
	if (hi < 1.5f)
		KRNL->ChangeState(new MenuState());
}
void GameState::HandleInput(CMD* cmd)
{

	switch(cmd->cmd)
	{
	case 126:
	{
				D3DXVECTOR4* derp = (D3DXVECTOR4*)cmd->extra;
				KRNL->GetCameraPtr()->update(derp->x, derp->y);
				delete cmd->extra;
				break;
	}
	case 124:
	{
				//int pause = 0;
				exit(10);
				break;
	}
		case 1:
			if ((char*)cmd->extra == (char*)1)
				fwd = true;
			else
				fwd = false;
			break;
		case 2:
			if ((char*)cmd->extra == (char*)1)
				lft = true;
			else
				lft = false;
 			break;
		case 3:
			if ((char*)cmd->extra == (char*)1)
				bak = true;
			else
				bak = false;
			break;
		case 4:
			if ((char*)cmd->extra == (char*)1)
				rgt = true;
			else
				rgt = false;
			break;
		case 5:
			if ((char*)cmd->extra == (char*)1)
				up = true;
			else
				up = false;	
			break;
		case 6:
			if ((char*)cmd->extra == (char*)1)
				dwn = true;
			else
				dwn = false;
			break;
		case 7:
			if ((char*)cmd->extra == (char*)1)
				mvup = true;
			else
				mvup = false;
			break;
		case 8:
			if ((char*)cmd->extra == (char*)1)
				mvlt = true;
			else
				mvlt = false;
			break;
		case 9:
			if ((char*)cmd->extra == (char*)1)
				mvdn = true;
			else
				mvdn = false;
			break;
		case 10:
			if ((char*)cmd->extra == (char*)1)
				mvrt = true;
			else
				mvrt = false;
			break;
	};

	
}
void GameState::Leave()
{

}

void GameState::PenalizePlayers()
{
	bool inpath;

	//Check all players to all path rectangles
	//If player sphere is not completely inside any of the path rectangles
	//then send that player back to the start position
	for (int i = 0; i != KRNL->m_Players.size(); i++)
	{
		inpath = false;

		for (int j = 0; j != KRNL->m_Paths.size(); j++)
		{
			if (CONTACT->mDetector.SphereInsideAABB(KRNL->m_Players[i]->mBoundingVolume, KRNL->m_Paths[j]->mBoundingVolume))
				inpath = true;
		}

		if (!inpath)
		{
			KRNL->m_Players[i]->SetPosition(StartPosition);
		}
	}
}