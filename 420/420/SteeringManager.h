#pragma once

#include "BaseAIEntity.h"
//#include "Random.h"

class SteeringManager
{
private:
	Entity*			SBase;
	D3DXVECTOR3		m_SteeringForce;
	D3DXVECTOR3		maxForce;
	D3DXVECTOR3		maxTurnRate;
	D3DXVECTOR3		m_Wander;
	D3DXVECTOR3		TargetPos;
	double			wanderRadius;
	double			wanderJitter;
	double			wanderDistance;

public:
	SteeringManager()
	{
		SBase				= NULL;
		maxForce			= D3DXVECTOR3(10, 0, 10);
		maxTurnRate			= D3DXVECTOR3(3, 0, 3);
		m_Wander			= D3DXVECTOR3(0, 0, 0);
		m_SeekWeight		= 3;
		m_FleeWeight		= 3;
		m_WallWeight		= 5;
		wanderRadius		= 10;
		wanderJitter		= 4;
		wanderDistance		= 10;
		m_WanderWeight		= (FLOAT)0.98f;
		m_ArriveWeight		= 3;
		m_SpecialWeight		= (FLOAT)0.4f;
		m_ObstacleWeight	= 5;
	}

	SteeringManager(Entity* owner)
	{
		SBase				= owner;
		maxForce			= D3DXVECTOR3(10, 0, 10);
		maxTurnRate			= D3DXVECTOR3(3, 0, 3);
		m_Wander			= D3DXVECTOR3(0, 0, 0);
		m_SeekWeight		= 4;
		m_FleeWeight		= 4;
		m_WallWeight		= 4;
		wanderRadius		= 10;
		wanderJitter		= 4;
		wanderDistance		= 10;
		m_WanderWeight		= 1;
		m_ArriveWeight		= 4;
		m_SpecialWeight		= (FLOAT)0.4f;
		m_ObstacleWeight	= 4;
	}

public:
	~SteeringManager()
	{
		maxForce			= D3DXVECTOR3(0, 0, 0);
		maxTurnRate			= D3DXVECTOR3(0, 0, 0);
		m_SeekWeight		= 0;
		m_FleeWeight		= 0;
		m_WallWeight		= 0;
		wanderRadius		= 0;
		wanderJitter		= 0;
		wanderDistance		= 0;
		m_WanderWeight		= 0;
		m_ArriveWeight		= 0;
		m_SpecialWeight		= 0;
		m_ObstacleWeight	= 0;
	}

public:
	D3DXVECTOR3		CalculateSForce(int ID, Entity* target);

	///Steering Algorithms
public:
	///Seek
	D3DXVECTOR3		Seek(D3DXVECTOR3 TargetPos);
	D3DXVECTOR3		Seek(Entity* Target);
	D3DXVECTOR3		GetSeek(D3DXVECTOR3 target);			///Used to return a vector with Seek, OA, & WA 

	///Flee
	D3DXVECTOR3		Flee(D3DXVECTOR3 TargetPos);
	D3DXVECTOR3		Flee(Entity* Target);
	D3DXVECTOR3		GetFlee(D3DXVECTOR3 target);			///Flee

	///Wander
	//D3DXVECTOR3 Wander();
	D3DXVECTOR3		GetWander();							///Wander

	///Arrive
	D3DXVECTOR3		Arrive(Entity* Target);
	D3DXVECTOR3		Arrive(D3DXVECTOR3 TargetPos);
	D3DXVECTOR3		GertArrive(D3DXVECTOR3 target);			///Arrive

	///Pursue
	D3DXVECTOR3		Pursue(Entity* Target);					///Pursue

	///Evade
	D3DXVECTOR3		Evade(Entity* Target);					///Evade

	///Hide
	/*vector3 Hide(BaseAIEntity* Target);					///Hide
	D3DXVECTOR3		Hide(vector3 TargetPos);
	D3DXVECTOR3		GetHide();*/

	///Wall Avoidance
	//D3DXVECTOR3	WallAvoidance() const;					///Wall

	///Obstacle Avoidance
	D3DXVECTOR3		ObstacleAvoidance() const;				///Obstacle
	bool			Test(Entity* e) const;

	///Entity Avoidance
	D3DXVECTOR3		EntityAvoidance();						///Entity

	///Coordinate Space
	//D3DXVECTOR3	PointToWorld(vector3 world);
	//D3DXVECTOR3	PointToLocal(vector3 local);

	///Create Feelers
	//void			CreateFeeler() const;

	///Wander helpers
	double			GetWanderRadius() const;
	double			GetWanderJitter() const;
	double			GetWanderDistance() const;

	///Steering Weights
private:
	FLOAT			m_SeekWeight;
	FLOAT			m_FleeWeight;
	FLOAT			m_WallWeight;
	FLOAT			m_ArriveWeight;
	FLOAT			m_HideWeight;
	FLOAT			m_WanderWeight;
	FLOAT			m_SpecialWeight;
	FLOAT			m_ObstacleWeight;
};

