#include "PhysicsInterface.h"
#include "collision function.h"
#include "Contact.h"
#include "Entity.h"

enum Axis{ Xa, Ya, Za };

PhysicsInterface* PhysicsInterface::s_Instance = 0;

PhysicsInterface* PhysicsInterface::Instance()
{
	if (!s_Instance)
		s_Instance = new PhysicsInterface();

	return s_Instance;
}

PhysicsInterface::PhysicsInterface()
{
	mCollisions = new CollisionData();
}

RigidBody* PhysicsInterface::CreateRigidBody(Entity* Master, V3 Position, float Radius, V3 Halfsize, float mass)
{
 	V3 Heading(1, 0, 0);
	BoundingVolume* BV = new BoundingVolume(Radius, Halfsize);
	RigidBody* temp = new RigidBody(mass, 1, Halfsize, 100.0f, 10.f);
	BoundingVolumeNode* BVN = new BoundingVolumeNode(Master, BV);
	temp->SetVolume(BVN);
	mCollisionVolumes.push_back(BVN);
	return temp;
}

//Remove Collision Volume
void PhysicsInterface::RemoveCollisionVolume(Entity* Body)
{
	for (unsigned i = 0; i < mCollisionVolumes.size(); i++)
	{
		if (mCollisionVolumes[i]->mBody = Body)
		{
			mCollisionVolumes[i] = NULL;
			delete mCollisionVolumes[i];
			mCollisionVolumes.erase(mCollisionVolumes.begin() + i);
		}
	}
}

//Update Physics Engine
void PhysicsInterface::UpdatePhysics(float dt)
{
	GenertateContacts();
	ResolveContacts();
}

//Generate Contacts
void PhysicsInterface::GenertateContacts()
{
	for (unsigned i = 0; i < mCollisionVolumes.size() - 1; i++)
		for (unsigned j = i + 1; j < mCollisionVolumes.size(); j++)
		if (i != j)
			if (mCollisionVolumes[i]->mVolume->mShape != 1 || mCollisionVolumes[j]->mVolume->mShape != 1)
				if (mCollisions->DetectCollision(mCollisionVolumes[i], mCollisionVolumes[j]))
					mCollisions->GenerateContact(mCollisionVolumes[i], mCollisionVolumes[j]);
}	

//Resolve Contacts
void PhysicsInterface::ResolveContacts()
{
	mCollisions->RsolveContacts();
}
