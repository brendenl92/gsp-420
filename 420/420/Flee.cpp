#include "Flee.h"
#include "AIManager.h"

Flee::Flee()
: BaseState()
{
	BaseName = "Flee";
}

Flee::Flee(AISM* f, int m_ID)
: BaseState(f)
{
	m_AISM		= f;
	m_ownerID	= m_ID;
	BaseName	= "Flee";
}

Flee::~Flee()
{
	m_ownerID	= NULL;
}

void Flee::Enter()
{

}

void Flee::Execute(double dt)
{

}

void Flee::Exit()
{

}

void Flee::HandleMsg(AI_Mail message)
{
	switch (message.m_Msg)
	{
	case _OnPatrol:
	{
					  ///On Patrol....
					  AIM->GetBaseAIEntity()[message.m_Reciever]->cs_ID = 1;
					  m_AISM->Transition("Wander");
					  break;
	}
	case _LockedOn:
	{
					  ///Locked On....
					  AIM->GetBaseAIEntity()[message.m_Reciever]->cs_ID = 2;
					  m_AISM->Transition("Attack");
					  break;
	}
	case _FallingBack:
	{
					  ///Falling Back....
					  AIM->GetBaseAIEntity()[message.m_Reciever]->cs_ID = 3;
					  break;
	}
	default:
					  break;
	}
}