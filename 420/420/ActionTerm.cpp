#include "ActionTerm.h"

ActionTerm::ActionTerm(int i, int t, char c_O)
{
	m_AS = ActionSet(i, t, c_O);
}

ActionTerm::ActionTerm(ActionTerm& m_AT)
{
	m_AS = m_AT.GetSet();
}

ActionSet ActionTerm::GetSet()
{
	return m_AS;
}

bool ActionTerm::Test()
{
	return m_AS.IsMet(); 
}

/*What I'm doing here is finding out if the two sets are met or not*/
bool AndAction::Test()
{
	if (m_Pair[0]->Test() == true && m_Pair[1]->Test() == true)
		return true;
	else
		return false;

	/*bool last_result, result, final_result;

	for (int i = 0; i != m_Pair.size(); i++)
	{
		result = m_Pair[i]->Test();

		//Has there been a last result?? (first iteration)
		if (last_result == NULL && i != m_Pair.size())
			last_result = result;
		//Does this result match last result??
		else
		{
			if (result == true)
			{
				if (result == last_result)
					final_result = true;
				else
					final_result = false;
			}
			else
				final_result = false;
		}
	}

	return final_result;*/
}

bool OrAction::Test()
{
	if (m_Pair[0]->Test() == true || m_Pair[1]->Test() == true)
		return true;
	else
		return false;
}
