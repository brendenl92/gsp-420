#pragma once
#include <chrono>
#include <ctime>
class Clock
{
	float elapsedTime;
	float dt;

	std::chrono::time_point<std::chrono::system_clock> start;
	std::chrono::time_point<std::chrono::system_clock> frame;

public:
	Clock();
	~Clock();
	void initialize();
	void beginFrame();
	void endFrame();
	float getTimeElapsed();
	float getDT();
};

