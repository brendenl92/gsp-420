#pragma once

#include <vector>
#include <d3d9.h>
#include "../inc/d3dx9.h"
#include "BaseComponent.h"
#include "AI_Interface.h"
#include "RigidBody.h"
#include "TreeDeeComponent.h"

enum type
{
	AIENABLE		= 0x00000001,
	PHYSICSENABLE	= 0x00000002,
	GRAPHICSENABLE	= 0x00000004
};

using namespace std;

class Entity
{

	unsigned int			id;
	D3DXVECTOR3				position;
	D3DXVECTOR3				velocity;
	D3DXQUATERNION			orientation;
	double					mass;

	D3DXVECTOR3				Lacceleration;
	D3DXQUATERNION			omega;
	D3DXMATRIX				transform;

	bool					mRigidEnabled;

	vector<BaseComponent*>	components;

public:
	//components
	BaseAIEntity*			AI_Base;
	RigidBody*				rigidbody;

public:

	//Codi added this
	BoundingVolumeNode*		mBoundingVolume;
	//Add a rigid Body component to this entity
	void					AddRigidBody(float  mass, int shape, D3DXVECTOR3 halfsize, float dampening);
	//Tells this entity that it has a rigid body
	void					EnableRigidBody(){ mRigidEnabled = true; }
	//Update all rigid body components in this entity
	void					UpdateRigidBody();
	//Return true if this entity is rigid enabled
	bool					RigidEnabled();
	//Get RigidBody
	RigidBody*				GetRigidBody() const;
	//RigidBody*				GetRigidBody() const;

	//Codi added this (problem bro!)
	void					AddVelocity(D3DXVECTOR3 vel){ velocity += vel; }

	//Bounding Volume
	void					AddBoundingVolume(float Radius, D3DXVECTOR3 Halfsize = D3DXVECTOR3(0, 0, 0));

	//Accessors
	D3DXMATRIX				GetTransform(){ return transform; }
	D3DXVECTOR3				GetLAcceleration(){ return Lacceleration; }
	D3DXQUATERNION			GetOrientation(){ return orientation; }

	Entity(void);
	~Entity(void);

	void					addComponent(BaseComponent* addition);

	inline void				SetPosition(D3DXVECTOR3 pos){ position = pos; }
	inline void				SetVelocity(D3DXVECTOR3 vel){ velocity = vel; }
	inline void				SetMass(float mass){ this->mass = mass; }
	inline void				SetID(unsigned int id){ this->id = id; }

	inline D3DXVECTOR3		GetPosition(){ return position; }
	inline D3DXVECTOR3		GetVelocity(){ return velocity; }
	inline D3DXQUATERNION	GetRotation(){ return orientation; }
	inline int				GetID() { return id; }

public:
	void					CreateComponent(char type, char AI_type,float m, float d, int shape, D3DXVECTOR3 h_size, D3DXVECTOR3 pos, D3DXVECTOR3 vel);
	void					Update(float dt);
	void					Shutdown();
};

