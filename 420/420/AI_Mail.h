/*
Created By: Bryan O'Dell
Created On: August 2nd, 2014

Used as a based message, that all AI Entities will use in 
communicating with each other.
*/
#pragma once

#include <math.h>

enum AI_Message{
	_None,
	_OnPatrol,
	_LockedOn,
	_FallingBack
};

class AI_Mail
{
public:
	int		m_Reciever, m_Sender;
	double	m_Delay;
	AI_Message m_Msg;
	void*	m_ExtraInfo;

public:
	AI_Mail(int r, int s, double d, int msg, void* info);
	~AI_Mail()
	{
		m_Reciever = 0, m_Sender = 0, m_Delay = 0, m_Msg = (AI_Message)0, m_ExtraInfo;
	}
};