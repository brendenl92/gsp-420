#include "GraphicsCore.h"

#define GUI GUICore::Instance()

class GUICore
{
public:
	static GUICore* Instance();
	void Initialize(),
		StartUp(),
		ShutDown(),
		Run(),
		Draw(),
		CreateButton();

	inline void isMouseDown(char* MouseDown){ IsMouseDown = MouseDown; }
	inline void SetMousePos(D3DXVECTOR4* MousePos){ this->MousePos = MousePos; }

private:
	D3DXMATRIX mMatrix;

	RECT ButtonRect,
		HoverButtonRect;

	char* IsMouseDown;
	D3DXVECTOR4* MousePos;
};