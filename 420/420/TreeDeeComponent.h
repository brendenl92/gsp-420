#pragma once
#include "BaseComponent.h"
#include "Mesh.h"

class TreeDeeComponent :
	public BaseComponent
{
	UINT mesh;
	D3DXVECTOR3 Scale;
public:
	TreeDeeComponent();
	~TreeDeeComponent();

	virtual void initialize(void* info);
	virtual void update(Entity* entity);
	virtual void shutdown();
	
	void setScale(D3DXVECTOR3 scale);
};

