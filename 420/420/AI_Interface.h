#pragma once

#include "AIManager.h"

#define AIF AI_Interface::Instance()

//forward declaration
class Entity;

class AI_Interface
{
public:
	AI_Interface();
	~AI_Interface() {}

public:
	static AI_Interface* Instance();

public:
	void			Init();
	BaseAIEntity*	CreateAIEntity(int type, Entity* e);
	void			Shutdown();
	void			Update(float dt, Entity* e);

private:
	static AI_Interface*	s_Instance;
		
};

