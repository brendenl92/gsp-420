#pragma once

#include "BaseComponent.h"
#include <d3d9.h>
#include "../inc/d3dx9.h"

typedef D3DXVECTOR3 V3;
typedef D3DXQUATERNION Qu;
typedef D3DXMATRIX M4;

class BoundingVolumeNode;

class RigidBody
{

protected:
	//Variables
	float	mDampening;
	float   mAngularDampening;
	float	mInvMass;
	float	mMaxSpeed;
	float	mMaxTurn;
	V3		mForceAccum;
	V3      mTorque;
	M4      mInvInertialTensor;
	M4      mInvInertialTensorWorld;
	bool	mAwake;

public:
	BoundingVolumeNode* mVolume;

public:

	//Constructor
	RigidBody() {}
	RigidBody(float  mass, int shape, V3 halfsize, float dampening, float max_speed = 0);

	~RigidBody() {}


	//Methods
	/*virtual */void initialize(void* info) {}
	/*virtual */void update(Entity* entity);
	/*virtual */void update(float dt, M4& transformation, Qu orientation, V3& velocity, Qu& angularvelocity, V3& Lacceleration);
	/*virtual */void shutdown() {}

	virtual bool isRigidBody();

	void	InertialTensor(int shape, V3 halfsize, float mass);

	void	AddForce(V3 force);
	void	AddTorqueAtPoint(M4& transform, V3& force, V3& point);
	void	AddTorqueAtBodyPoint(M4& transform, V3& force, V3& point);
	void	CalculateDerivedData(M4& Transform, V3 position, Qu orientation);
	void	ClearAccumulator();

private:
	V3		GetPosition(M4 Transform);
	void	SetPosition(M4& transform, V3 position);

	V3		GetPointInWorldSpace(M4& Transform, V3 point);

	void	SetInvInertTensWorld(M4& transform, M4 invinerten);

	void	SetTransform(M4& transform, V3 position, Qu orientation);

public:
	float	Mass();
	V3		GetHalfSize();
	void	SetVolume(BoundingVolumeNode* BoundingVolumeNode){ mVolume = BoundingVolumeNode;}
};