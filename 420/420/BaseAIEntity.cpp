#include "Entity.h"
#include "Kernel.h"
#include "AIManager.h"
#include "BaseAIEntity.h"
#include "SteeringManager.h"

int BaseAIEntity::counter = 0;
BaseAIEntity::BaseAIEntity()
{
	hp				= 100;
	ai_ID			= counter++;
	cs_ID			= 0;
	m_asm			= new AISM();
	maxSpeed		= 12;
	m_MyPlan		= new ActionPlan();
	p_mSteering		= new SteeringManager();
	i_Threshhold	= maxSpeed / 4;
	o_Threshhold	= i_Threshhold + 0.0001;
}

BaseAIEntity::BaseAIEntity(char t, Entity* e)
{
	hp				= 100;
	type			= t;
	ai_ID			= counter++;
	cs_ID			= 0;
	m_asm			= new AISM();
	maxSpeed		= 12;
	m_MyPlan		= new ActionPlan();
	p_mSteering		= new SteeringManager(e);
	i_Threshhold	= maxSpeed / 4;
	o_Threshhold	= i_Threshhold + 0.0001;
}

BaseAIEntity::~BaseAIEntity()
{
	counter--;
}

/* This function is going to be used to determine how many (if any) enemies
are in the AI Entities comfort zone*/
void BaseAIEntity::Zone(Entity* e)
{
	//Clear the enemy map and vector
	//m_EPrior.clear();
	m_Enemies.clear();

	/*First we need to make sure that the outer threshold is either at
	at the limit of our AI's view or between the limit and our comfort
	zone*/
	double vmag = D3DXVec3Length(&e->GetVelocity());
	if (vmag >= maxSpeed)
		o_Threshhold = maxSpeed;
	else
		o_Threshhold = vmag;

	/*Next we need to determine what is within our ranges...*/
	for (auto i = KRNL->GetEntities().begin(); i != KRNL->GetEntities().end(); i++)
	{
		//Test and see if AI is testing against itself
		if ((*i)->AI_Base->ai_ID == ai_ID)
			continue;
		else
		{
			//Get distance to target
			double distance = abs(D3DXVec3Length(&(e->GetPosition() - (*i)->GetPosition())));

			//Is the distance greater or smaller than my vision range
			if (distance > o_Threshhold)
				continue;
			else
			{
				//So the object is a AI Bot
				if (type & AIBOTS)
				{
					//Are they in the AI's comfort zone
					if (distance <= i_Threshhold && distance > 0)
					{
						//m_EPrior[(*i)->AI_Base] = distance, THREAT;
						m_Enemies.front() = (*i)->AI_Base;
					}
					else if (distance <= o_Threshhold && distance > (o_Threshhold / 2))
					{
						m_Enemies.push_back((*i)->AI_Base);
					}
					else
						continue;
				}
				else if (type & NPCS)
				{
					//insert into map as harmless
					//m_EPrior[(*i)->AI_Base] = distance, HARMLESS;
					continue;
				}
				else
					continue;
			}
		}
	}

}

/* This will be used to update the AI Entity*/
void BaseAIEntity::Update(float dt, Entity* e1)
{
	//Check our Zone and see what going on...
	Zone(e1);

	//Consult our Action Plan to see what the best course of action is...
	int best = 0;
	best = m_MyPlan->Consult();

	//Create a new message based off of what our Consulting message is...
	AI_Mail letter = AI_Mail((this)->ai_ID, (this)->ai_ID, dt, best, NULL);

	//From here send off the message, and Transition to a new state if needed....
	m_asm->HandleMsg(letter);

	//Calculate our new steering forced based off of our decision
	for (auto i = KRNL->GetEntities().begin(); i != KRNL->GetEntities().end(); i++)
	{
		for (auto j = KRNL->GetEntities().begin() + 1; j != KRNL->GetEntities().end(); j++)
		{
			for (int k = 0; k != (*i)->AI_Base->getAIEnemies().size(); k++)
			{
				if ((*j)->AI_Base->ai_ID == (*i)->AI_Base->getAIEnemies()[k]->ai_ID)
				{
					e1->SetVelocity(p_mSteering->CalculateSForce(best, (*j)));
				}
			}
		}
	}

	//update the fsm
	m_asm->Update(dt);

	//update velocity
	Truncate(e1->GetVelocity(), maxSpeed);
}

/* This will be used to shutdown the AI Entity, and decrement the static 
counter*/
void BaseAIEntity::Shutdown()
{
	//Nullify the Enemies vector
	vector<BaseAIEntity*> temp1;
	temp1.swap(m_Enemies);
	m_Enemies.clear();

	//Nullify Priority map
	/*map<BaseAIEntity*, double, char> temp2;
	temp2.swap(m_EPrior);
	m_EPrior.clear();*/
}

void BaseAIEntity::HandleMsg(AI_Mail message)
{
	m_asm->HandleMsg(message);
}

D3DXVECTOR3 BaseAIEntity::Truncate(D3DXVECTOR3 s, float ms)
{
	if (D3DXVec3Length(&s) > ms)
		s = *D3DXVec3Normalize(&s, &s) * ms;
	return s;
}

vector<BaseAIEntity*> BaseAIEntity::getAIEnemies()
{
	return m_Enemies;
}
