#include "ActionPlan.h"


ActionPlan::ActionPlan()
{
}


ActionPlan::~ActionPlan()
{
	vector<ActionNode*> temp;
	temp.swap(m_Plan);
	m_Plan.empty();
}

void ActionPlan::AddNode(ActionNode an)
{
	m_Plan.push_back(&an);
}

/*Here the Idea is to go through the plan an test all possible plans
and choose the best plan. Based off that plan I'll return the ID to
the state we are wanting to transition into.*/
int ActionPlan::Consult()
{
	int best = 999999;

	for (int i = 0; i != m_Plan.size(); i++)
	{
		if (m_Plan[i]->Test() == true && i < best)
		{
			best = i;
		}
	}

	return m_Plan[best]->GetEndState();
}
