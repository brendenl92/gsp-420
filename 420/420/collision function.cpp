#include"collision function.h"
#include "RigidBody.h"
#include "Contact.h"

V3 Cross(V3& Va, V3& Vb)
{
	V3 temp;
	D3DXVec3Cross(&temp, &Va, &Vb);
	return temp;
}

float GetM(V3 V, int dim)
{
	//Get vec x, or y, or z
	switch (dim)
	{
	case 0:
		return V.x;
	case 1:
		return V.y;
	case 2:
		return V.z;
	}

	return V.x;
}



void SetM(V3& V, int dim, float value)
{
	//Get vec x, or y, or z
	switch (dim)
	{
	case 0:
		V.x = value;
	case 1:
		V.y = value;
	case 2:
		V.z = value;
	}
}





//static inline V3 contactPoint(V3 &pOne, V3 &dOne, float oneSize, V3 &pTwo, V3 &dTwo, float twoSize,
//	// If this is true, and the contact point is outside
//	// the edge (in the case of an edge-face contact) then
//	// we use one's midpoint, otherwise we use two's.
//	bool useOne)
//{
//	V3 toSt, cOne, cTwo;
//	float dpStaOne, dpStaTwo, dpOneTwo, smOne, smTwo;
//	float denom, mua, mub;
//
//	smOne = D3DXVec3LengthSq(&dOne);
//	smTwo = D3DXVec3LengthSq(&dTwo);
//	dpOneTwo = D3DXVec3Dot(&dTwo, &dOne);
//
//	toSt = pOne - pTwo;
//	dpStaOne = D3DXVec3Dot(&dOne, &toSt);
//	dpStaTwo = D3DXVec3Dot(&dTwo, &toSt);
//
//	denom = smOne * smTwo - dpOneTwo * dpOneTwo;
//
//	// Zero denominator indicates parrallel lines
//	if (abs(denom) < 0.0001f) {
//		return useOne ? pOne : pTwo;
//	}
//
//	mua = (dpOneTwo * dpStaTwo - smTwo * dpStaOne) / denom;
//	mub = (smOne * dpStaTwo - dpOneTwo * dpStaOne) / denom;
//
//	// If either of the edges has the nearest point out
//	// of bounds, then the edges aren't crossed, we have
//	// an edge-face contact. Our point is on the edge, which
//	// we know from the useOne parameter.
//	if (mua > oneSize ||
//		mua < -oneSize ||
//		mub > twoSize ||
//		mub < -twoSize)
//	{
//		return useOne ? pOne : pTwo;
//	}
//	else
//	{
//		cOne = pOne + dOne * mua;
//		cTwo = pTwo + dTwo * mub;
//
//		return cOne * 0.5 + cTwo * 0.5;
//	}
//}


//Get Axis of rotation matrix
V3 GetAxisR(M4& RotM, int Col)
{
	V3 Axis;

	for (int i = 0; i < 3; i++)
		SetM(Axis, i, RotM.m[i][Col]);

	return Axis;
}



//Clip to axis
void ClipToAxis(D3DXMATRIX& M)
{
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			if (abs(M.m[i][j]) > .9f)
			{
				if (M.m[i][j] > 0)
					M.m[i][j] = 1;
				else
					M.m[i][j] = -1;
			}
			if (abs(M.m[i][j]) < .05f)
			{
				if (M.m[i][j] > 0)
					M.m[i][j] = 0;
			}
		}
	}
}


//Invert Z Axis
void InvertZAxis(D3DXMATRIX& RotM)
{
	for (unsigned int i = 0; i < 3; i++)
		RotM.m[i][2] *= -1;
}



//Create orthonormal basis based on a forward vector
void OrthnormBasFwdVec(M4& RotM, V3& Heading)
{
	static int CountL = 0;
	CountL++;
	
	V3 hed;
	float magSqr = D3DXVec3LengthSq(&Heading);
 	D3DXVec3Normalize(&hed, &Heading);

	if (abs(magSqr) > 0.001f)
	{
		V3 Ax, Ay, Az;

		Ax = hed;
		Ay = V3(0, 1, 0);

		Az = Cross(Ax, Ay);
		//Ay = Cross(Ax, Az);

		//seting rotation matrix
		for (int i = 0; i < 3; i++)
		{
			RotM.m[i][0] = GetM(Ax, i);
			RotM.m[i][1] = GetM(Ay, i);
			RotM.m[i][2] = GetM(Az, i);
		}

	/*	Az = hed;
		Ay = V3(0, 1, 0);

		Ax = Cross(Ay, Az);
		Ay = Cross(Ax, Az);

		for (int i = 0; i < 3; i++)
		{
			RotM.m[i][0] = GetM(Ax, i);
			RotM.m[i][1] = GetM(Ay, i);
			RotM.m[i][2] = GetM(Az, i);
		}*/

	}
	else
		Heading = V3(0,0,0);

	/*if (CountL >= 1000)
	{
 
		system("cls");

		cout << "\nVelocity" <<
			"\nX, Y, Z\n"
			<< Heading.x << ", " << Heading.y << ", " << Heading.z << endl;
			

		cout << "\nRotation" << 
			"\nX: ("    
			<< RotM.m[0][0] << ", " << RotM.m[1][0] << ", " << RotM.m[2][0] << ")\n"
			<< "Y: ("
			<< RotM.m[0][1] << ", " << RotM.m[1][1] << ", " << RotM.m[2][1] << ")\n"
			<< "Z: ("
			<< RotM.m[0][2] << ", " << RotM.m[2][1] << ", " << RotM.m[2][2] << ")\n";

		CountL = 0;
	}*/
}


