#include "AIManager.h"

AIManager* AIManager::s_Instance = 0;
AIManager* AIManager::Instance()
{
	if (!s_Instance)
		s_Instance = new AIManager;
	return s_Instance;
}

void AIManager::Init()
{

}

BaseAIEntity* AIManager::CreateAIEntity(char type, Entity* e)
{
	BaseAIEntity *base = new BaseAIEntity(type, e);

	//Here I'll want to add data plans to the Action plan for this 
	//AI Entity, based off of type of course
	if (type & AIBOTS)
	{
		//AndAction an1 = AndAction(ActionTerm(base->getAIEnimies().size(), 0, 4), ActionTerm(base->hp, 50, 4));
		base->m_MyPlan->AddNode(ActionNode(1, &AndAction(ActionTerm(base->getAIEnemies().size(), 0, 4), ActionTerm(base->hp, 50, 4)), 3));
		base->m_MyPlan->AddNode(ActionNode(1, &AndAction(ActionTerm(base->getAIEnemies().size(), 0, 4), ActionTerm(base->hp, 50, 1)), 2));
	}
	else if (type & NPCS)
	{

	}

	return base;
}

//void AIManager::AddPlan(int ai_ID, int p_ID, int r_ID)
//{
//
//}
//
//int AIManager::Consequence(double i, double t, enum option)
//{
//
//}

void AIManager::Shutdown()
{
	vector<BaseAIEntity*> temp;
	temp.swap(m_vAIEntities);
	m_vAIEntities.clear();

	delete s_Instance;
	s_Instance = NULL;
}

vector<BaseAIEntity*> AIManager::GetBaseAIEntity()
{
	return m_vAIEntities;
}